import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class ReviewsScreen extends StatefulWidget {
  @override
  _ReviewsScreenState createState() => _ReviewsScreenState();
}

class _ReviewsScreenState extends State<ReviewsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Theme.of(context).primaryColorDark,
            size: 18,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Theme.of(context).primaryColor,
              size: 18,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
        title: Center(
          child: Text(
            "Reviews",
            style: TextStyle(
              color: Theme.of(context).primaryColorDark,
              fontWeight: FontWeight.w600,
              fontSize: 16,
              fontFamily: 'Poppins',
              letterSpacing: 0.3,
              height: 1,
            ),
          ),
        ),
      ),
      body: SafeArea(
        top: true,
        bottom: true,
        child: Container(
          color: Theme.of(context).primaryColor,
          width: double.infinity,
          height: double.infinity,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 20,
                        right: 20,
                        bottom: 10,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25,
                            ),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Micheal Johnson",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                              right: 15,
                                              top: 6,
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                              ),
                                              child: Text(
                                                "4.2",
                                                style: TextStyle(
                                                    height: 1,
                                                    fontSize: 12,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 6,
                                            ),
                                            child: RatingBar.builder(
                                              initialRating: 3,
                                              minRating: 1,
                                              unratedColor: Theme.of(context)
                                                  .primaryColorDark,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 16,
                                              itemPadding: EdgeInsets.all(0),
                                              itemBuilder: (context, _) => Icon(
                                                Icons.star,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                                size: 14,
                                              ),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "5 April, 17",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 10,
                                    ),
                                    child: Column(
                                      children: [
                                        Text(
                                          "It is the best place for you to find and order delicious food made with love! On this platform, you can choose from a variety of cuisines and menus",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 14,
                                            height: 1.4,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color:
                                                Theme.of(context).buttonColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25,
                            ),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Micheal Johnson",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                              right: 15,
                                              top: 6,
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                              ),
                                              child: Text(
                                                "4.2",
                                                style: TextStyle(
                                                    height: 1,
                                                    fontSize: 12,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 6,
                                            ),
                                            child: RatingBar.builder(
                                              initialRating: 3,
                                              minRating: 1,
                                              unratedColor: Theme.of(context)
                                                  .primaryColorDark,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 16,
                                              itemPadding: EdgeInsets.all(0),
                                              itemBuilder: (context, _) => Icon(
                                                Icons.star,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                                size: 14,
                                              ),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "5 April, 17",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 10,
                                    ),
                                    child: Column(
                                      children: [
                                        Text(
                                          "It is the best place for you to find and order delicious food made with love! On this platform, you can choose from a variety of cuisines and menus",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 14,
                                            height: 1.4,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color:
                                                Theme.of(context).buttonColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25,
                            ),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Micheal Johnson",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                              right: 15,
                                              top: 6,
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                              ),
                                              child: Text(
                                                "4.2",
                                                style: TextStyle(
                                                    height: 1,
                                                    fontSize: 12,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 6,
                                            ),
                                            child: RatingBar.builder(
                                              initialRating: 3,
                                              minRating: 1,
                                              unratedColor: Theme.of(context)
                                                  .primaryColorDark,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 16,
                                              itemPadding: EdgeInsets.all(0),
                                              itemBuilder: (context, _) => Icon(
                                                Icons.star,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                                size: 14,
                                              ),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "5 April, 17",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 10,
                                    ),
                                    child: Column(
                                      children: [
                                        Text(
                                          "It is the best place for you to find and order delicious food made with love! On this platform, you can choose from a variety of cuisines and menus",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 14,
                                            height: 1.4,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color:
                                                Theme.of(context).buttonColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25,
                            ),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Micheal Johnson",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                              right: 15,
                                              top: 6,
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                              ),
                                              child: Text(
                                                "4.2",
                                                style: TextStyle(
                                                    height: 1,
                                                    fontSize: 12,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 6,
                                            ),
                                            child: RatingBar.builder(
                                              initialRating: 3,
                                              minRating: 1,
                                              unratedColor: Theme.of(context)
                                                  .primaryColorDark,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 16,
                                              itemPadding: EdgeInsets.all(0),
                                              itemBuilder: (context, _) => Icon(
                                                Icons.star,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                                size: 14,
                                              ),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "5 April, 17",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 10,
                                    ),
                                    child: Column(
                                      children: [
                                        Text(
                                          "It is the best place for you to find and order delicious food made with love! On this platform, you can choose from a variety of cuisines and menus",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 14,
                                            height: 1.4,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color:
                                                Theme.of(context).buttonColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25,
                            ),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Micheal Johnson",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                              right: 15,
                                              top: 6,
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                              ),
                                              child: Text(
                                                "4.2",
                                                style: TextStyle(
                                                    height: 1,
                                                    fontSize: 12,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 6,
                                            ),
                                            child: RatingBar.builder(
                                              initialRating: 3,
                                              minRating: 1,
                                              unratedColor: Theme.of(context)
                                                  .primaryColorDark,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 16,
                                              itemPadding: EdgeInsets.all(0),
                                              itemBuilder: (context, _) => Icon(
                                                Icons.star,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                                size: 14,
                                              ),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "5 April, 17",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 10,
                                    ),
                                    child: Column(
                                      children: [
                                        Text(
                                          "It is the best place for you to find and order delicious food made with love! On this platform, you can choose from a variety of cuisines and menus",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 14,
                                            height: 1.4,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color:
                                                Theme.of(context).buttonColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25,
                            ),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Micheal Johnson",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                              right: 15,
                                              top: 6,
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                              ),
                                              child: Text(
                                                "4.2",
                                                style: TextStyle(
                                                    height: 1,
                                                    fontSize: 12,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 6,
                                            ),
                                            child: RatingBar.builder(
                                              initialRating: 3,
                                              minRating: 1,
                                              unratedColor: Theme.of(context)
                                                  .primaryColorDark,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 16,
                                              itemPadding: EdgeInsets.all(0),
                                              itemBuilder: (context, _) => Icon(
                                                Icons.star,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                                size: 14,
                                              ),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "5 April, 17",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 10,
                                    ),
                                    child: Column(
                                      children: [
                                        Text(
                                          "It is the best place for you to find and order delicious food made with love! On this platform, you can choose from a variety of cuisines and menus",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 14,
                                            height: 1.4,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color:
                                                Theme.of(context).buttonColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25,
                            ),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Micheal Johnson",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                              right: 15,
                                              top: 6,
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                              ),
                                              child: Text(
                                                "4.2",
                                                style: TextStyle(
                                                    height: 1,
                                                    fontSize: 12,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 6,
                                            ),
                                            child: RatingBar.builder(
                                              initialRating: 3,
                                              minRating: 1,
                                              unratedColor: Theme.of(context)
                                                  .primaryColorDark,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 16,
                                              itemPadding: EdgeInsets.all(0),
                                              itemBuilder: (context, _) => Icon(
                                                Icons.star,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                                size: 14,
                                              ),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "5 April, 17",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 10,
                                    ),
                                    child: Column(
                                      children: [
                                        Text(
                                          "It is the best place for you to find and order delicious food made with love! On this platform, you can choose from a variety of cuisines and menus",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 14,
                                            height: 1.4,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color:
                                                Theme.of(context).buttonColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25,
                            ),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Micheal Johnson",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                              right: 15,
                                              top: 6,
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                              ),
                                              child: Text(
                                                "4.2",
                                                style: TextStyle(
                                                    height: 1,
                                                    fontSize: 12,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 6,
                                            ),
                                            child: RatingBar.builder(
                                              initialRating: 3,
                                              minRating: 1,
                                              unratedColor: Theme.of(context)
                                                  .primaryColorDark,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 16,
                                              itemPadding: EdgeInsets.all(0),
                                              itemBuilder: (context, _) => Icon(
                                                Icons.star,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                                size: 14,
                                              ),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "5 April, 17",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 10,
                                    ),
                                    child: Column(
                                      children: [
                                        Text(
                                          "It is the best place for you to find and order delicious food made with love! On this platform, you can choose from a variety of cuisines and menus",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 14,
                                            height: 1.4,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color:
                                                Theme.of(context).buttonColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25,
                            ),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Micheal Johnson",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                              right: 15,
                                              top: 6,
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                              ),
                                              child: Text(
                                                "4.2",
                                                style: TextStyle(
                                                    height: 1,
                                                    fontSize: 12,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 6,
                                            ),
                                            child: RatingBar.builder(
                                              initialRating: 3,
                                              minRating: 1,
                                              unratedColor: Theme.of(context)
                                                  .primaryColorDark,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 16,
                                              itemPadding: EdgeInsets.all(0),
                                              itemBuilder: (context, _) => Icon(
                                                Icons.star,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                                size: 14,
                                              ),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "5 April, 17",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 10,
                                    ),
                                    child: Column(
                                      children: [
                                        Text(
                                          "It is the best place for you to find and order delicious food made with love! On this platform, you can choose from a variety of cuisines and menus",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 14,
                                            height: 1.4,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color:
                                                Theme.of(context).buttonColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25,
                            ),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Micheal Johnson",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                              right: 15,
                                              top: 6,
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                              ),
                                              child: Text(
                                                "4.2",
                                                style: TextStyle(
                                                    height: 1,
                                                    fontSize: 12,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 6,
                                            ),
                                            child: RatingBar.builder(
                                              initialRating: 3,
                                              minRating: 1,
                                              unratedColor: Theme.of(context)
                                                  .primaryColorDark,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 16,
                                              itemPadding: EdgeInsets.all(0),
                                              itemBuilder: (context, _) => Icon(
                                                Icons.star,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                                size: 14,
                                              ),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "5 April, 17",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 10,
                                    ),
                                    child: Column(
                                      children: [
                                        Text(
                                          "It is the best place for you to find and order delicious food made with love! On this platform, you can choose from a variety of cuisines and menus",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 14,
                                            height: 1.4,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color:
                                                Theme.of(context).buttonColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25,
                            ),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Micheal Johnson",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                              right: 15,
                                              top: 6,
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                              ),
                                              child: Text(
                                                "4.2",
                                                style: TextStyle(
                                                    height: 1,
                                                    fontSize: 12,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 6,
                                            ),
                                            child: RatingBar.builder(
                                              initialRating: 3,
                                              minRating: 1,
                                              unratedColor: Theme.of(context)
                                                  .primaryColorDark,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 16,
                                              itemPadding: EdgeInsets.all(0),
                                              itemBuilder: (context, _) => Icon(
                                                Icons.star,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                                size: 14,
                                              ),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "5 April, 17",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 10,
                                    ),
                                    child: Column(
                                      children: [
                                        Text(
                                          "It is the best place for you to find and order delicious food made with love! On this platform, you can choose from a variety of cuisines and menus",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 14,
                                            height: 1.4,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color:
                                                Theme.of(context).buttonColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25,
                            ),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Micheal Johnson",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                              right: 15,
                                              top: 6,
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                              ),
                                              child: Text(
                                                "4.2",
                                                style: TextStyle(
                                                    height: 1,
                                                    fontSize: 12,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 6,
                                            ),
                                            child: RatingBar.builder(
                                              initialRating: 3,
                                              minRating: 1,
                                              unratedColor: Theme.of(context)
                                                  .primaryColorDark,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 16,
                                              itemPadding: EdgeInsets.all(0),
                                              itemBuilder: (context, _) => Icon(
                                                Icons.star,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                                size: 14,
                                              ),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "5 April, 17",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 10,
                                    ),
                                    child: Column(
                                      children: [
                                        Text(
                                          "It is the best place for you to find and order delicious food made with love! On this platform, you can choose from a variety of cuisines and menus",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 14,
                                            height: 1.4,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color:
                                                Theme.of(context).buttonColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
