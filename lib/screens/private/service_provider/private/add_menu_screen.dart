import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodpanda/screens/private/service_provider/private/add_items_screen.dart';
import 'package:foodpanda/screens/private/service_provider/private/main_dashboard.dart';

class AddMenuScreen extends StatefulWidget {
  @override
  _AddMenuScreenState createState() => _AddMenuScreenState();
}

class _AddMenuScreenState extends State<AddMenuScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Theme.of(context).primaryColorDark,
            size: 18,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Theme.of(context).primaryColor,
              size: 18,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
        title: Center(
          child: Text(
            "Add Menu",
            style: TextStyle(
              color: Theme.of(context).primaryColorDark,
              fontWeight: FontWeight.w600,
              fontSize: 16,
              fontFamily: 'Poppins',
              letterSpacing: 0.3,
              height: 1,
            ),
          ),
        ),
      ),
      body: SafeArea(
        top: true,
        bottom: true,
        child: Container(
          color: Theme.of(context).cardColor,
          width: double.infinity,
          height: double.infinity,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 20,
                        right: 20,
                        bottom: 20,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                              top: 15,
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        right: 10,
                                        top: 2,
                                      ),
                                      child: Text(
                                        "Add Category",
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      padding: const EdgeInsets.only(
                                        top: 4,
                                        left: 4,
                                        right: 4,
                                        bottom: 4,
                                      ),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(50),
                                          topLeft: Radius.circular(50),
                                          bottomRight: Radius.circular(50),
                                          bottomLeft: Radius.circular(50),
                                        ),
                                        color: Theme.of(context).primaryColor,
                                      ),
                                      child: Icon(
                                        Icons.add,
                                        color: Theme.of(context).accentColor,
                                        size: 18,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 15,
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.black,
                                borderRadius: BorderRadius.circular(12),
                              ),
                              width: double.infinity,
                              padding: EdgeInsets.only(
                                left: 15,
                                right: 15,
                                bottom: 25,
                                top: 15,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 0,
                                      bottom: 10,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                              right: 10,
                                            ),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                TextFormField(
                                                  style: new TextStyle(
                                                    fontFamily: 'Poppins',
                                                    height: 1,
                                                    fontSize: 14,
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                  ),
                                                  decoration: InputDecoration(
                                                    border: UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                    disabledBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    errorBorder: UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                    enabledBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    focusedErrorBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    focusedBorder:
                                                        UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColorDark),
                                                    ),
                                                    contentPadding:
                                                        EdgeInsets.only(
                                                      left: 0,
                                                      bottom: 0,
                                                      top: 0,
                                                      right: 0,
                                                    ),
                                                    hintText: "Add Category",
                                                    hintStyle: TextStyle(
                                                      fontSize: 16,
                                                      color: Theme.of(context)
                                                          .buttonColor,
                                                    ),
                                                    fillColor: Theme.of(context)
                                                        .primaryColorDark,
                                                    hoverColor:
                                                        Theme.of(context)
                                                            .primaryColorDark,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                right: 10,
                                                top: 2,
                                              ),
                                              child: Text(
                                                "Edit",
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  height: 1,
                                                  letterSpacing: 0.3,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w400,
                                                  color: Theme.of(context)
                                                      .primaryColorDark,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              padding: const EdgeInsets.only(
                                                top: 5,
                                                left: 5,
                                                right: 5,
                                                bottom: 5,
                                              ),
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                                color:
                                                    Theme.of(context).cardColor,
                                              ),
                                              child: Icon(
                                                Icons.edit,
                                                color: Theme.of(context)
                                                    .accentColor,
                                                size: 18,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                      top: 10,
                                      bottom: 10,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Item",
                                          style: TextStyle(
                                            fontSize: 16,
                                            height: 1,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                          ),
                                        ),
                                        Text(
                                          "Price",
                                          style: TextStyle(
                                            fontSize: 16,
                                            height: 1,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                      top: 10,
                                      bottom: 5,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Container(
                                              padding: const EdgeInsets.only(
                                                top: 4,
                                                left: 4,
                                                right: 4,
                                                bottom: 4,
                                              ),
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                                color:
                                                    Theme.of(context).cardColor,
                                              ),
                                              child: Icon(
                                                Icons.add,
                                                color: Theme.of(context)
                                                    .accentColor,
                                                size: 18,
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                left: 10,
                                                top: 2,
                                              ),
                                              child: Text(
                                                "Add New Item",
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  height: 1,
                                                  letterSpacing: 0.3,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w400,
                                                  color: Theme.of(context)
                                                      .primaryColorDark,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            right: 10,
                                          ),
                                          child: Container(
                                            padding: const EdgeInsets.only(
                                              top: 4,
                                              left: 4,
                                              right: 4,
                                              bottom: 4,
                                            ),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(50),
                                                topLeft: Radius.circular(50),
                                                bottomRight:
                                                    Radius.circular(50),
                                                bottomLeft: Radius.circular(50),
                                              ),
                                              color:
                                                  Theme.of(context).cardColor,
                                            ),
                                            child: Icon(
                                              Icons.check,
                                              color:
                                                  Theme.of(context).accentColor,
                                              size: 18,
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                              right: 10,
                                            ),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                TextFormField(
                                                  style: new TextStyle(
                                                    fontFamily: 'Poppins',
                                                    height: 1,
                                                    fontSize: 14,
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                  ),
                                                  decoration: InputDecoration(
                                                    border: UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                    disabledBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    errorBorder: UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                    enabledBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    focusedErrorBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    focusedBorder:
                                                        UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColorDark),
                                                    ),
                                                    contentPadding:
                                                        EdgeInsets.only(
                                                      left: 0,
                                                      bottom: 0,
                                                      top: 0,
                                                      right: 0,
                                                    ),
                                                    hintText: "Add Item",
                                                    hintStyle: TextStyle(
                                                      fontSize: 16,
                                                      color: Theme.of(context)
                                                          .buttonColor,
                                                    ),
                                                    fillColor: Theme.of(context)
                                                        .primaryColorDark,
                                                    hoverColor:
                                                        Theme.of(context)
                                                            .primaryColorDark,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                              right: 0,
                                            ),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                TextFormField(
                                                  style: new TextStyle(
                                                    fontFamily: 'Poppins',
                                                    height: 1,
                                                    fontSize: 14,
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                  ),
                                                  decoration: InputDecoration(
                                                    border: UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                    disabledBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    errorBorder: UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                    enabledBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    focusedErrorBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    focusedBorder:
                                                        UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColorDark),
                                                    ),
                                                    contentPadding:
                                                        EdgeInsets.only(
                                                      left: 0,
                                                      bottom: 0,
                                                      top: 0,
                                                      right: 0,
                                                    ),
                                                    hintText: "Add Price",
                                                    hintStyle: TextStyle(
                                                      fontSize: 16,
                                                      color: Theme.of(context)
                                                          .buttonColor,
                                                    ),
                                                    fillColor: Theme.of(context)
                                                        .primaryColorDark,
                                                    hoverColor:
                                                        Theme.of(context)
                                                            .primaryColorDark,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 15,
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.black,
                                borderRadius: BorderRadius.circular(12),
                              ),
                              width: double.infinity,
                              padding: EdgeInsets.only(
                                left: 15,
                                right: 15,
                                bottom: 25,
                                top: 15,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 0,
                                      bottom: 10,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                              right: 10,
                                            ),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                TextFormField(
                                                  initialValue: "Ice Cream",
                                                  style: new TextStyle(
                                                    fontFamily: 'Poppins',
                                                    height: 1,
                                                    fontSize: 14,
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                  ),
                                                  decoration: InputDecoration(
                                                    border: UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                    disabledBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    errorBorder: UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                    enabledBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    focusedErrorBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    focusedBorder:
                                                        UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColorDark),
                                                    ),
                                                    contentPadding:
                                                        EdgeInsets.only(
                                                      left: 0,
                                                      bottom: 0,
                                                      top: 0,
                                                      right: 0,
                                                    ),
                                                    hintText: "Add Category",
                                                    hintStyle: TextStyle(
                                                      fontSize: 16,
                                                      color: Theme.of(context)
                                                          .buttonColor,
                                                    ),
                                                    fillColor: Theme.of(context)
                                                        .primaryColorDark,
                                                    hoverColor:
                                                        Theme.of(context)
                                                            .primaryColorDark,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                right: 10,
                                                top: 2,
                                              ),
                                              child: Text(
                                                "Edit",
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  height: 1,
                                                  letterSpacing: 0.3,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w400,
                                                  color: Theme.of(context)
                                                      .primaryColorDark,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              padding: const EdgeInsets.only(
                                                top: 5,
                                                left: 5,
                                                right: 5,
                                                bottom: 5,
                                              ),
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                                color:
                                                    Theme.of(context).cardColor,
                                              ),
                                              child: Icon(
                                                Icons.edit,
                                                color: Theme.of(context)
                                                    .accentColor,
                                                size: 18,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                      top: 10,
                                      bottom: 10,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Item",
                                          style: TextStyle(
                                            fontSize: 16,
                                            height: 1,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                          ),
                                        ),
                                        Text(
                                          "Price",
                                          style: TextStyle(
                                            fontSize: 16,
                                            height: 1,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                      top: 10,
                                      bottom: 5,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Container(
                                              padding: const EdgeInsets.only(
                                                top: 4,
                                                left: 4,
                                                right: 4,
                                                bottom: 4,
                                              ),
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(50),
                                                  topLeft: Radius.circular(50),
                                                  bottomRight:
                                                      Radius.circular(50),
                                                  bottomLeft:
                                                      Radius.circular(50),
                                                ),
                                                color:
                                                    Theme.of(context).cardColor,
                                              ),
                                              child: Icon(
                                                Icons.add,
                                                color: Theme.of(context)
                                                    .accentColor,
                                                size: 18,
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                left: 10,
                                                top: 2,
                                              ),
                                              child: Text(
                                                "Add New Item",
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  height: 1,
                                                  letterSpacing: 0.3,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w400,
                                                  color: Theme.of(context)
                                                      .primaryColorDark,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 0,
                                      bottom: 10,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                right: 10,
                                              ),
                                              child: Container(
                                                padding: const EdgeInsets.only(
                                                  top: 5,
                                                  left: 5,
                                                  right: 5,
                                                  bottom: 5,
                                                ),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.only(
                                                    topRight:
                                                        Radius.circular(50),
                                                    topLeft:
                                                        Radius.circular(50),
                                                    bottomRight:
                                                        Radius.circular(50),
                                                    bottomLeft:
                                                        Radius.circular(50),
                                                  ),
                                                  color: Theme.of(context)
                                                      .cardColor,
                                                ),
                                                child: Image.asset(
                                                  "assets/images/minus-sign.png",
                                                  color: Theme.of(context)
                                                      .accentColor,
                                                  width: 15,
                                                  height: 15,
                                                ),
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        AddItemScreen(),
                                                  ),
                                                );
                                              },
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                  right: 10,
                                                ),
                                                child: Container(
                                                  padding:
                                                      const EdgeInsets.only(
                                                    top: 5,
                                                    left: 5,
                                                    right: 5,
                                                    bottom: 5,
                                                  ),
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                      topRight:
                                                          Radius.circular(50),
                                                      topLeft:
                                                          Radius.circular(50),
                                                      bottomRight:
                                                          Radius.circular(50),
                                                      bottomLeft:
                                                          Radius.circular(50),
                                                    ),
                                                    color: Theme.of(context)
                                                        .cardColor,
                                                  ),
                                                  child: Icon(
                                                    Icons.edit,
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                    size: 18,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 180,
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                  right: 10,
                                                ),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    TextFormField(
                                                      initialValue: "Choclate",
                                                      style: new TextStyle(
                                                        fontFamily: 'Poppins',
                                                        height: 1,
                                                        fontSize: 14,
                                                        letterSpacing: 0.3,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        color: Theme.of(context)
                                                            .primaryColorDark,
                                                      ),
                                                      decoration:
                                                          InputDecoration(
                                                        border: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        disabledBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        errorBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        enabledBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        focusedErrorBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        focusedBorder:
                                                            UnderlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColorDark),
                                                        ),
                                                        contentPadding:
                                                            EdgeInsets.only(
                                                          left: 0,
                                                          bottom: 0,
                                                          top: 0,
                                                          right: 0,
                                                        ),
                                                        hintText: "Add Item",
                                                        hintStyle: TextStyle(
                                                          fontSize: 16,
                                                          color:
                                                              Theme.of(context)
                                                                  .buttonColor,
                                                        ),
                                                        fillColor: Theme.of(
                                                                context)
                                                            .primaryColorDark,
                                                        hoverColor: Theme.of(
                                                                context)
                                                            .primaryColorDark,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                right: 10,
                                              ),
                                              child: Text(
                                                "Rs.",
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  height: 1,
                                                  letterSpacing: 0.3,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w400,
                                                  color: Theme.of(context)
                                                      .primaryColorDark,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 30,
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                  right: 10,
                                                ),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    TextFormField(
                                                      initialValue: "50",
                                                      style: new TextStyle(
                                                        fontFamily: 'Poppins',
                                                        height: 1,
                                                        fontSize: 14,
                                                        letterSpacing: 0.3,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        color: Theme.of(context)
                                                            .primaryColorDark,
                                                      ),
                                                      decoration:
                                                          InputDecoration(
                                                        border: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        disabledBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        errorBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        enabledBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        focusedErrorBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        focusedBorder:
                                                            UnderlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColorDark),
                                                        ),
                                                        contentPadding:
                                                            EdgeInsets.only(
                                                          left: 0,
                                                          bottom: 0,
                                                          top: 0,
                                                          right: 0,
                                                        ),
                                                        hintText: "Add Item",
                                                        hintStyle: TextStyle(
                                                          fontSize: 16,
                                                          color:
                                                              Theme.of(context)
                                                                  .buttonColor,
                                                        ),
                                                        fillColor: Theme.of(
                                                                context)
                                                            .primaryColorDark,
                                                        hoverColor: Theme.of(
                                                                context)
                                                            .primaryColorDark,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 0,
                                      bottom: 10,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                right: 10,
                                              ),
                                              child: Container(
                                                padding: const EdgeInsets.only(
                                                  top: 5,
                                                  left: 5,
                                                  right: 5,
                                                  bottom: 5,
                                                ),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.only(
                                                    topRight:
                                                        Radius.circular(50),
                                                    topLeft:
                                                        Radius.circular(50),
                                                    bottomRight:
                                                        Radius.circular(50),
                                                    bottomLeft:
                                                        Radius.circular(50),
                                                  ),
                                                  color: Theme.of(context)
                                                      .cardColor,
                                                ),
                                                child: Image.asset(
                                                  "assets/images/minus-sign.png",
                                                  color: Theme.of(context)
                                                      .accentColor,
                                                  width: 15,
                                                  height: 15,
                                                ),
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        AddItemScreen(),
                                                  ),
                                                );
                                              },
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                  right: 10,
                                                ),
                                                child: Container(
                                                  padding:
                                                      const EdgeInsets.only(
                                                    top: 5,
                                                    left: 5,
                                                    right: 5,
                                                    bottom: 5,
                                                  ),
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                      topRight:
                                                          Radius.circular(50),
                                                      topLeft:
                                                          Radius.circular(50),
                                                      bottomRight:
                                                          Radius.circular(50),
                                                      bottomLeft:
                                                          Radius.circular(50),
                                                    ),
                                                    color: Theme.of(context)
                                                        .cardColor,
                                                  ),
                                                  child: Icon(
                                                    Icons.edit,
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                    size: 18,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 180,
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                  right: 10,
                                                ),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    TextFormField(
                                                      initialValue: "Choclate",
                                                      style: new TextStyle(
                                                        fontFamily: 'Poppins',
                                                        height: 1,
                                                        fontSize: 14,
                                                        letterSpacing: 0.3,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        color: Theme.of(context)
                                                            .primaryColorDark,
                                                      ),
                                                      decoration:
                                                          InputDecoration(
                                                        border: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        disabledBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        errorBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        enabledBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        focusedErrorBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        focusedBorder:
                                                            UnderlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColorDark),
                                                        ),
                                                        contentPadding:
                                                            EdgeInsets.only(
                                                          left: 0,
                                                          bottom: 0,
                                                          top: 0,
                                                          right: 0,
                                                        ),
                                                        hintText: "Add Item",
                                                        hintStyle: TextStyle(
                                                          fontSize: 16,
                                                          color:
                                                              Theme.of(context)
                                                                  .buttonColor,
                                                        ),
                                                        fillColor: Theme.of(
                                                                context)
                                                            .primaryColorDark,
                                                        hoverColor: Theme.of(
                                                                context)
                                                            .primaryColorDark,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                right: 10,
                                              ),
                                              child: Text(
                                                "Rs.",
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  height: 1,
                                                  letterSpacing: 0.3,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w400,
                                                  color: Theme.of(context)
                                                      .primaryColorDark,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 30,
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                  right: 10,
                                                ),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    TextFormField(
                                                      initialValue: "50",
                                                      style: new TextStyle(
                                                        fontFamily: 'Poppins',
                                                        height: 1,
                                                        fontSize: 14,
                                                        letterSpacing: 0.3,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        color: Theme.of(context)
                                                            .primaryColorDark,
                                                      ),
                                                      decoration:
                                                          InputDecoration(
                                                        border: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        disabledBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        errorBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        enabledBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        focusedErrorBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                        focusedBorder:
                                                            UnderlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColorDark),
                                                        ),
                                                        contentPadding:
                                                            EdgeInsets.only(
                                                          left: 0,
                                                          bottom: 0,
                                                          top: 0,
                                                          right: 0,
                                                        ),
                                                        hintText: "Add Item",
                                                        hintStyle: TextStyle(
                                                          fontSize: 16,
                                                          color:
                                                              Theme.of(context)
                                                                  .buttonColor,
                                                        ),
                                                        fillColor: Theme.of(
                                                                context)
                                                            .primaryColorDark,
                                                        hoverColor: Theme.of(
                                                                context)
                                                            .primaryColorDark,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            right: 10,
                                          ),
                                          child: Container(
                                            padding: const EdgeInsets.only(
                                              top: 4,
                                              left: 4,
                                              right: 4,
                                              bottom: 4,
                                            ),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(50),
                                                topLeft: Radius.circular(50),
                                                bottomRight:
                                                    Radius.circular(50),
                                                bottomLeft: Radius.circular(50),
                                              ),
                                              color:
                                                  Theme.of(context).cardColor,
                                            ),
                                            child: Icon(
                                              Icons.check,
                                              color:
                                                  Theme.of(context).accentColor,
                                              size: 18,
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                              right: 10,
                                            ),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                TextFormField(
                                                  style: new TextStyle(
                                                    fontFamily: 'Poppins',
                                                    height: 1,
                                                    fontSize: 14,
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                  ),
                                                  decoration: InputDecoration(
                                                    border: UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                    disabledBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    errorBorder: UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                    enabledBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    focusedErrorBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    focusedBorder:
                                                        UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColorDark),
                                                    ),
                                                    contentPadding:
                                                        EdgeInsets.only(
                                                      left: 0,
                                                      bottom: 0,
                                                      top: 0,
                                                      right: 0,
                                                    ),
                                                    hintText: "Add Item",
                                                    hintStyle: TextStyle(
                                                      fontSize: 16,
                                                      color: Theme.of(context)
                                                          .buttonColor,
                                                    ),
                                                    fillColor: Theme.of(context)
                                                        .primaryColorDark,
                                                    hoverColor:
                                                        Theme.of(context)
                                                            .primaryColorDark,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                              right: 0,
                                            ),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                TextFormField(
                                                  style: new TextStyle(
                                                    fontFamily: 'Poppins',
                                                    height: 1,
                                                    fontSize: 14,
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                  ),
                                                  decoration: InputDecoration(
                                                    border: UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                    disabledBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    errorBorder: UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                    enabledBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    focusedErrorBorder:
                                                        UnderlineInputBorder(
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorDark)),
                                                    focusedBorder:
                                                        UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColorDark),
                                                    ),
                                                    contentPadding:
                                                        EdgeInsets.only(
                                                      left: 0,
                                                      bottom: 0,
                                                      top: 0,
                                                      right: 0,
                                                    ),
                                                    hintText: "Add Price",
                                                    hintStyle: TextStyle(
                                                      fontSize: 16,
                                                      color: Theme.of(context)
                                                          .buttonColor,
                                                    ),
                                                    fillColor: Theme.of(context)
                                                        .primaryColorDark,
                                                    hoverColor:
                                                        Theme.of(context)
                                                            .primaryColorDark,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    left: 20,
                    right: 20,
                    bottom: 20,
                    top: 20,
                  ),
                  color: Theme.of(context).primaryColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: double.infinity,
                        height: 50,
                        child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.resolveWith<Color>(
                              (Set<MaterialState> states) {
                                return states.contains(MaterialState.pressed)
                                    ? Theme.of(context).accentColor
                                    : Theme.of(context).accentColor;
                              },
                            ),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                                side: BorderSide(
                                  color: Theme.of(context).accentColor,
                                ),
                              ),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => MainDashboardScreen(1),
                              ),
                            );
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Save",
                                style: TextStyle(
                                  color: Theme.of(context).primaryColorDark,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: 'Poppins',
                                  letterSpacing: 0.5,
                                  fontSize: 16,
                                  height: 1,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
