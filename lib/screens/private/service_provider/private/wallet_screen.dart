import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WalletScreen extends StatefulWidget {
  @override
  _WalletScreenState createState() => _WalletScreenState();
}

class _WalletScreenState extends State<WalletScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Theme.of(context).primaryColorDark,
            size: 18,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Theme.of(context).primaryColor,
              size: 18,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
        title: Center(
          child: Text(
            "Wallet",
            style: TextStyle(
              color: Theme.of(context).primaryColorDark,
              fontWeight: FontWeight.w600,
              fontSize: 16,
              fontFamily: 'Poppins',
              letterSpacing: 0.3,
              height: 1,
            ),
          ),
        ),
      ),
      body: SafeArea(
        top: true,
        bottom: true,
        child: Container(
          color: Theme.of(context).primaryColor,
          width: double.infinity,
          height: double.infinity,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 20,
                        right: 20,
                        bottom: 20,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25,
                              bottom: 10,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "Avaliable Balance",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 20,
                                    height: 1,
                                    letterSpacing: 0.3,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w400,
                                    color: Theme.of(context).primaryColorDark,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 0,
                              bottom: 15,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "Rs. 400",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 29,
                                    height: 1,
                                    letterSpacing: 0.3,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600,
                                    color: Theme.of(context).accentColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            color: Theme.of(context).cardColor,
                            padding: EdgeInsets.only(
                              top: 0,
                              left: 0,
                              right: 0,
                              bottom: 0,
                            ),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      right: 10,
                                    ),
                                    child: SizedBox(
                                      height: 50,
                                      child: ElevatedButton(
                                        style: ButtonStyle(
                                          backgroundColor: MaterialStateProperty
                                              .resolveWith<Color>(
                                            (Set<MaterialState> states) {
                                              return states.contains(
                                                      MaterialState.pressed)
                                                  ? Theme.of(context)
                                                      .primaryColorDark
                                                  : Theme.of(context)
                                                      .primaryColorDark;
                                            },
                                          ),
                                          shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              side: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                              ),
                                            ),
                                          ),
                                        ),
                                        onPressed: () {},
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                left: 0,
                                              ),
                                              child: Text(
                                                "Send To Bank",
                                                style: TextStyle(
                                                  color: Theme.of(context)
                                                      .accentColor,
                                                  fontWeight: FontWeight.w600,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.5,
                                                  fontSize: 16,
                                                  height: 1,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      left: 10,
                                    ),
                                    child: SizedBox(
                                      height: 50,
                                      child: ElevatedButton(
                                        style: ButtonStyle(
                                          backgroundColor: MaterialStateProperty
                                              .resolveWith<Color>(
                                            (Set<MaterialState> states) {
                                              return states.contains(
                                                      MaterialState.pressed)
                                                  ? Theme.of(context)
                                                      .accentColor
                                                  : Theme.of(context)
                                                      .accentColor;
                                            },
                                          ),
                                          shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              side: BorderSide(
                                                color: Theme.of(context)
                                                    .accentColor,
                                              ),
                                            ),
                                          ),
                                        ),
                                        onPressed: () {
                                          // Navigator.push(
                                          //   context,
                                          //   MaterialPageRoute(
                                          //     builder: (context) =>
                                          //         ChatScreenTwo(),
                                          //   ),
                                          // );
                                        },
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                left: 0,
                                              ),
                                              child: Text(
                                                "Add Money",
                                                style: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorDark,
                                                  fontWeight: FontWeight.w600,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.5,
                                                  fontSize: 16,
                                                  height: 1,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25,
                              bottom: 15,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  "Recent",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontSize: 20,
                                    height: 1,
                                    letterSpacing: 0.3,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w400,
                                    color: Theme.of(context).primaryColorDark,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(
                              //     builder: (context) =>
                              //         OrderStatusAcceptedScreen(),
                              //   ),
                              // );
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(
                                top: 10,
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(6),
                                    topLeft: Radius.circular(6),
                                    bottomRight: Radius.circular(6),
                                    bottomLeft: Radius.circular(6),
                                  ),
                                  color: Theme.of(context).cardColor,
                                ),
                                width: double.infinity,
                                padding: const EdgeInsets.only(
                                  top: 15,
                                  left: 15,
                                  right: 15,
                                  bottom: 15,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 0,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Order #212217",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "Rs. 400",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 16,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Rs. 400",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                              color:
                                                  Theme.of(context).accentColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 10,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "3 items | 30 Jun 2018, 11:58 am",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "Online",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 14,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Earnings",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(
                              //     builder: (context) =>
                              //         OrderStatusAcceptedScreen(),
                              //   ),
                              // );
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(
                                top: 10,
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(6),
                                    topLeft: Radius.circular(6),
                                    bottomRight: Radius.circular(6),
                                    bottomLeft: Radius.circular(6),
                                  ),
                                  color: Theme.of(context).cardColor,
                                ),
                                width: double.infinity,
                                padding: const EdgeInsets.only(
                                  top: 15,
                                  left: 15,
                                  right: 15,
                                  bottom: 15,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 0,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Order #212217",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "Rs. 400",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 16,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Rs. 400",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                              color:
                                                  Theme.of(context).accentColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 10,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "3 items | 30 Jun 2018, 11:58 am",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "Online",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 14,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Earnings",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(
                              //     builder: (context) =>
                              //         OrderStatusAcceptedScreen(),
                              //   ),
                              // );
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(
                                top: 10,
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(6),
                                    topLeft: Radius.circular(6),
                                    bottomRight: Radius.circular(6),
                                    bottomLeft: Radius.circular(6),
                                  ),
                                  color: Theme.of(context).cardColor,
                                ),
                                width: double.infinity,
                                padding: const EdgeInsets.only(
                                  top: 15,
                                  left: 15,
                                  right: 15,
                                  bottom: 15,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 0,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Order #212217",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "Rs. 400",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 16,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Rs. 400",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                              color:
                                                  Theme.of(context).accentColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 10,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "3 items | 30 Jun 2018, 11:58 am",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "COD",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 14,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Earnings",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(
                              //     builder: (context) =>
                              //         OrderStatusAcceptedScreen(),
                              //   ),
                              // );
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(
                                top: 10,
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(6),
                                    topLeft: Radius.circular(6),
                                    bottomRight: Radius.circular(6),
                                    bottomLeft: Radius.circular(6),
                                  ),
                                  color: Theme.of(context).cardColor,
                                ),
                                width: double.infinity,
                                padding: const EdgeInsets.only(
                                  top: 15,
                                  left: 15,
                                  right: 15,
                                  bottom: 15,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 0,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Order #212217",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "Rs. 400",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 16,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Rs. 400",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                              color:
                                                  Theme.of(context).accentColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 10,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "3 items | 30 Jun 2018, 11:58 am",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "Online",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 14,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Earnings",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(
                              //     builder: (context) =>
                              //         OrderStatusAcceptedScreen(),
                              //   ),
                              // );
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(
                                top: 10,
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(6),
                                    topLeft: Radius.circular(6),
                                    bottomRight: Radius.circular(6),
                                    bottomLeft: Radius.circular(6),
                                  ),
                                  color: Theme.of(context).cardColor,
                                ),
                                width: double.infinity,
                                padding: const EdgeInsets.only(
                                  top: 15,
                                  left: 15,
                                  right: 15,
                                  bottom: 15,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 0,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Order #212217",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "Rs. 400",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 16,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Rs. 400",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                              color:
                                                  Theme.of(context).accentColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 10,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "3 items | 30 Jun 2018, 11:58 am",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "Online",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 14,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Earnings",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(
                              //     builder: (context) =>
                              //         OrderStatusAcceptedScreen(),
                              //   ),
                              // );
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(
                                top: 10,
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(6),
                                    topLeft: Radius.circular(6),
                                    bottomRight: Radius.circular(6),
                                    bottomLeft: Radius.circular(6),
                                  ),
                                  color: Theme.of(context).cardColor,
                                ),
                                width: double.infinity,
                                padding: const EdgeInsets.only(
                                  top: 15,
                                  left: 15,
                                  right: 15,
                                  bottom: 15,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 0,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Order #212217",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "Rs. 400",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 16,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Rs. 400",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                              color:
                                                  Theme.of(context).accentColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 10,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "3 items | 30 Jun 2018, 11:58 am",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "COD",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 14,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Earnings",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(
                              //     builder: (context) =>
                              //         OrderStatusAcceptedScreen(),
                              //   ),
                              // );
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(
                                top: 10,
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(6),
                                    topLeft: Radius.circular(6),
                                    bottomRight: Radius.circular(6),
                                    bottomLeft: Radius.circular(6),
                                  ),
                                  color: Theme.of(context).cardColor,
                                ),
                                width: double.infinity,
                                padding: const EdgeInsets.only(
                                  top: 15,
                                  left: 15,
                                  right: 15,
                                  bottom: 15,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 0,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Order #212217",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "Rs. 400",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 16,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Rs. 400",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                              color:
                                                  Theme.of(context).accentColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 10,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "3 items | 30 Jun 2018, 11:58 am",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "Online",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 14,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Earnings",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(
                              //     builder: (context) =>
                              //         OrderStatusAcceptedScreen(),
                              //   ),
                              // );
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(
                                top: 10,
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(6),
                                    topLeft: Radius.circular(6),
                                    bottomRight: Radius.circular(6),
                                    bottomLeft: Radius.circular(6),
                                  ),
                                  color: Theme.of(context).cardColor,
                                ),
                                width: double.infinity,
                                padding: const EdgeInsets.only(
                                  top: 15,
                                  left: 15,
                                  right: 15,
                                  bottom: 15,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 0,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Order #212217",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "Rs. 400",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 16,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Rs. 400",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                              color:
                                                  Theme.of(context).accentColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 10,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "3 items | 30 Jun 2018, 11:58 am",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 15,
                                            ),
                                            child: Text(
                                              "COD",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 14,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Earnings",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 14,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color:
                                                  Theme.of(context).buttonColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
