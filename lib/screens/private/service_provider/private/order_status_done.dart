import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodpanda/screens/private/service_provider/private/chat_screen.dart';

class OrderStatusDoneScreen extends StatefulWidget {
  @override
  _OrderStatusDoneScreenState createState() => _OrderStatusDoneScreenState();
}

class _OrderStatusDoneScreenState extends State<OrderStatusDoneScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: true,
        bottom: true,
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                "assets/images/maps_dv.png",
              ),
              fit: BoxFit.cover,
            ),
          ),
          width: double.infinity,
          height: double.infinity,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 20,
                        left: 10,
                        bottom: 0,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: Image.asset(
                              "assets/images/close.png",
                              width: 15,
                              height: 15,
                              color: Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  color: Theme.of(context).primaryColor,
                  width: double.infinity,
                  padding: EdgeInsets.only(
                    top: 20,
                    left: 20,
                    right: 20,
                    bottom: 20,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 0,
                            ),
                            child: Text(
                              "Deliverman",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 16,
                                height: 1,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w400,
                                color: Theme.of(context).buttonColor,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 0,
                            ),
                            child: Text(
                              "Arriving In",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 16,
                                height: 1,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w400,
                                color: Theme.of(context).buttonColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 10,
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 0,
                              ),
                              child: Text(
                                "David Anderson",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 16,
                                  height: 1,
                                  letterSpacing: 0.3,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w500,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 0,
                              ),
                              child: Text(
                                "10 Mins",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 16,
                                  height: 1,
                                  letterSpacing: 0.3,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w500,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 10,
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 0,
                              ),
                              child: Text(
                                "+923242492566",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 16,
                                  height: 1,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w400,
                                  color: Theme.of(context).buttonColor,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 0,
                              ),
                              child: Text(
                                "32 Km Away",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 16,
                                  height: 1,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w400,
                                  color: Theme.of(context).buttonColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  color: Theme.of(context).cardColor,
                  padding: EdgeInsets.only(
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: SizedBox(
                          height: 55,
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.resolveWith<Color>(
                                (Set<MaterialState> states) {
                                  return states.contains(MaterialState.pressed)
                                      ? Theme.of(context).primaryColorDark
                                      : Theme.of(context).primaryColorDark;
                                },
                              ),
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(0),
                                  side: BorderSide(
                                    color: Theme.of(context).primaryColorDark,
                                  ),
                                ),
                              ),
                            ),
                            onPressed: () {},
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.call,
                                  size: 22,
                                  color: Theme.of(context).accentColor,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    left: 8,
                                  ),
                                  child: Text(
                                    "Call Now",
                                    style: TextStyle(
                                      color: Theme.of(context).accentColor,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: 'Poppins',
                                      letterSpacing: 0.5,
                                      fontSize: 16,
                                      height: 1,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: SizedBox(
                          height: 55,
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.resolveWith<Color>(
                                (Set<MaterialState> states) {
                                  return states.contains(MaterialState.pressed)
                                      ? Theme.of(context).accentColor
                                      : Theme.of(context).accentColor;
                                },
                              ),
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(0),
                                  side: BorderSide(
                                    color: Theme.of(context).accentColor,
                                  ),
                                ),
                              ),
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ChatScreenTwo(),
                                ),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.message,
                                  size: 22,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    left: 8,
                                  ),
                                  child: Text(
                                    "Message",
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColorDark,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: 'Poppins',
                                      letterSpacing: 0.5,
                                      fontSize: 16,
                                      height: 1,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
