import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodpanda/screens/private/service_provider/private/add_menu_screen.dart';

class AddProfileScreen extends StatefulWidget {
  @override
  _AddProfileScreenState createState() => _AddProfileScreenState();
}

class _AddProfileScreenState extends State<AddProfileScreen> {
  bool enableSwitch = true;
  FocusNode _emailFocusNode = new FocusNode();
  FocusNode _emailFocusNdddddode = new FocusNode();
  FocusNode _emailsFocusNode = new FocusNode();
  FocusNode _emailsFocusdNode = new FocusNode();
  FocusNode _emailsssFocusdNode = new FocusNode();
  FocusNode _emailsdddddssFocusdNode = new FocusNode();
  FocusNode _emailsdddddsssadasdFocusdNode = new FocusNode();
  FocusNode _emailsdddddsssdddddddadasdFocusdNode = new FocusNode();
  bool _value3 = false;
  bool _value4 = false;
  @override
  void initState() {
    super.initState();
    _emailFocusNode = FocusNode();
    _emailFocusNdddddode = FocusNode();
    _emailsFocusNode = FocusNode();
    _emailsFocusdNode = FocusNode();
    _emailsssFocusdNode = FocusNode();
    _emailsdddddssFocusdNode = FocusNode();
    _emailsdddddsssadasdFocusdNode = FocusNode();
    _emailsdddddsssdddddddadasdFocusdNode = FocusNode();
  }

  @override
  void dispose() {
    _emailFocusNode.dispose();
    _emailFocusNdddddode.dispose();
    _emailsFocusNode.dispose();
    _emailsFocusdNode.dispose();
    _emailsssFocusdNode.dispose();
    _emailsdddddssFocusdNode.dispose();
    _emailsdddddsssadasdFocusdNode.dispose();
    _emailsdddddsssdddddddadasdFocusdNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        top: true,
        bottom: true,
        child: Container(
          color: Theme.of(context).cardColor,
          width: double.infinity,
          height: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: double.infinity,
                height: 250,
                padding: const EdgeInsets.only(
                  left: 20,
                  right: 20,
                  top: 20,
                ),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                ),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.arrow_back_ios,
                            size: 22,
                            color: Theme.of(context).primaryColorDark,
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 40,
                      ),
                      child: Container(
                        padding: const EdgeInsets.only(
                          top: 20,
                          left: 20,
                          right: 20,
                          bottom: 20,
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(50),
                            topLeft: Radius.circular(50),
                            bottomRight: Radius.circular(50),
                            bottomLeft: Radius.circular(50),
                          ),
                          color: Theme.of(context).cardColor,
                        ),
                        child: Icon(
                          Icons.camera_alt_rounded,
                          color: Theme.of(context).accentColor,
                          size: 35,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 25,
                      ),
                      child: Text(
                        "Upload Image",
                        style: TextStyle(
                          fontSize: 22,
                          height: 1,
                          letterSpacing: 0.3,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w400,
                          color: Theme.of(context).primaryColorDark,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 0,
                      right: 0,
                      bottom: 0,
                      top: 0,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          padding: const EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                            top: 20,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 10,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Icon(
                                          Icons.message,
                                          size: 22,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            left: 10,
                                          ),
                                          child: Text(
                                            "Restaurant Details",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    TextFormField(
                                      focusNode: _emailFocusNode,
                                      autofocus: false,
                                      autocorrect: false,
                                      enableInteractiveSelection: true,
                                      onTap: () {
                                        setState(
                                          () {
                                            _emailFocusNode.requestFocus();
                                          },
                                        );
                                      },
                                      style: new TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                      ),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.only(
                                          top: 0,
                                          left: 0,
                                          right: 0,
                                          bottom: 0,
                                        ),
                                        labelText: "Restro Name",
                                        labelStyle: TextStyle(
                                          fontFamily: 'Poppins',
                                          color: _emailFocusNode.hasFocus
                                              ? Theme.of(context).accentColor
                                              : Theme.of(context)
                                                  .primaryColorDark,
                                          fontSize: 16,
                                        ),
                                        border: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        disabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        errorBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        focusedErrorBorder:
                                            UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Theme.of(context)
                                                        .primaryColorDark)),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Theme.of(context)
                                                  .primaryColorDark),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    TextFormField(
                                      focusNode: _emailsFocusNode,
                                      autofocus: false,
                                      autocorrect: false,
                                      enableInteractiveSelection: true,
                                      onTap: () {
                                        setState(
                                          () {
                                            _emailsFocusNode.requestFocus();
                                          },
                                        );
                                      },
                                      style: new TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                      ),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.only(
                                          top: 0,
                                          left: 0,
                                          right: 0,
                                          bottom: 0,
                                        ),
                                        labelText: "Registered Email Address",
                                        labelStyle: TextStyle(
                                          fontFamily: 'Poppins',
                                          color: _emailsFocusNode.hasFocus
                                              ? Theme.of(context).accentColor
                                              : Theme.of(context)
                                                  .primaryColorDark,
                                          fontSize: 16,
                                        ),
                                        border: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        disabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        errorBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        focusedErrorBorder:
                                            UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Theme.of(context)
                                                        .primaryColorDark)),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Theme.of(context)
                                                  .primaryColorDark),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    TextFormField(
                                      focusNode: _emailsFocusdNode,
                                      autofocus: false,
                                      autocorrect: false,
                                      enableInteractiveSelection: true,
                                      onTap: () {
                                        setState(
                                          () {
                                            _emailsFocusdNode.requestFocus();
                                          },
                                        );
                                      },
                                      style: new TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                      ),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.only(
                                          top: 0,
                                          left: 0,
                                          right: 0,
                                          bottom: 0,
                                        ),
                                        labelText: "Whatsapp Number",
                                        labelStyle: TextStyle(
                                          fontFamily: 'Poppins',
                                          color: _emailsFocusdNode.hasFocus
                                              ? Theme.of(context).accentColor
                                              : Theme.of(context)
                                                  .primaryColorDark,
                                          fontSize: 16,
                                        ),
                                        border: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        disabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        errorBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        focusedErrorBorder:
                                            UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Theme.of(context)
                                                        .primaryColorDark)),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Theme.of(context)
                                                  .primaryColorDark),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    TextFormField(
                                      focusNode: _emailsFocusdNode,
                                      autofocus: false,
                                      autocorrect: false,
                                      enableInteractiveSelection: true,
                                      onTap: () {
                                        setState(
                                          () {
                                            _emailsFocusdNode.requestFocus();
                                          },
                                        );
                                      },
                                      style: new TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                      ),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.only(
                                          top: 0,
                                          left: 0,
                                          right: 0,
                                          bottom: 0,
                                        ),
                                        labelText: "Phone Number",
                                        labelStyle: TextStyle(
                                          fontFamily: 'Poppins',
                                          color: _emailsFocusdNode.hasFocus
                                              ? Theme.of(context).accentColor
                                              : Theme.of(context)
                                                  .primaryColorDark,
                                          fontSize: 16,
                                        ),
                                        border: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        disabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        errorBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        focusedErrorBorder:
                                            UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Theme.of(context)
                                                        .primaryColorDark)),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Theme.of(context)
                                                  .primaryColorDark),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          color: Theme.of(context).primaryColor,
                          width: double.infinity,
                          padding: const EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                            top: 20,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 0,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Icon(
                                          Icons.location_on,
                                          size: 22,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            left: 10,
                                          ),
                                          child: Text(
                                            "Restro Address",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Change",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 16,
                                            height: 1,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w500,
                                            color:
                                                Theme.of(context).accentColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 5,
                                  left: 32,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      "It is the best place for you to find and order delicious food made",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontSize: 14,
                                        height: 1.4,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w400,
                                        color: Theme.of(context).buttonColor,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                            top: 20,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 10,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Icon(
                                          Icons.watch_later,
                                          size: 22,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            left: 10,
                                          ),
                                          child: Text(
                                            "Restaurant Working Hours",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                          right: 10,
                                        ),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            TextFormField(
                                              focusNode:
                                                  _emailsdddddsssdddddddadasdFocusdNode,
                                              autofocus: false,
                                              autocorrect: false,
                                              enableInteractiveSelection: true,
                                              onTap: () {
                                                setState(
                                                  () {
                                                    _emailsdddddsssdddddddadasdFocusdNode
                                                        .requestFocus();
                                                  },
                                                );
                                              },
                                              style: new TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                                fontSize: 18,
                                                fontFamily: 'Poppins',
                                              ),
                                              decoration: InputDecoration(
                                                contentPadding: EdgeInsets.only(
                                                  top: 0,
                                                  left: 0,
                                                  right: 0,
                                                  bottom: 0,
                                                ),
                                                labelText: "Opening Time",
                                                labelStyle: TextStyle(
                                                  fontFamily: 'Poppins',
                                                  color: _emailsdddddsssdddddddadasdFocusdNode
                                                          .hasFocus
                                                      ? Theme.of(context)
                                                          .accentColor
                                                      : Theme.of(context)
                                                          .primaryColorDark,
                                                  fontSize: 16,
                                                ),
                                                border: UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: Theme.of(context)
                                                            .primaryColorDark)),
                                                disabledBorder:
                                                    UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                errorBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: Theme.of(context)
                                                            .primaryColorDark)),
                                                enabledBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: Theme.of(context)
                                                            .primaryColorDark)),
                                                focusedErrorBorder:
                                                    UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                focusedBorder:
                                                    UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .primaryColorDark),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                          left: 10,
                                        ),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            TextFormField(
                                              focusNode: _emailFocusNdddddode,
                                              autofocus: false,
                                              autocorrect: false,
                                              enableInteractiveSelection: true,
                                              onTap: () {
                                                setState(
                                                  () {
                                                    _emailFocusNdddddode
                                                        .requestFocus();
                                                  },
                                                );
                                              },
                                              style: new TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                                fontSize: 18,
                                                fontFamily: 'Poppins',
                                              ),
                                              decoration: InputDecoration(
                                                contentPadding: EdgeInsets.only(
                                                  top: 0,
                                                  left: 0,
                                                  right: 0,
                                                  bottom: 0,
                                                ),
                                                labelText: "Closing Time",
                                                labelStyle: TextStyle(
                                                  fontFamily: 'Poppins',
                                                  color: _emailFocusNdddddode
                                                          .hasFocus
                                                      ? Theme.of(context)
                                                          .accentColor
                                                      : Theme.of(context)
                                                          .primaryColorDark,
                                                  fontSize: 16,
                                                ),
                                                border: UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: Theme.of(context)
                                                            .primaryColorDark)),
                                                disabledBorder:
                                                    UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                errorBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: Theme.of(context)
                                                            .primaryColorDark)),
                                                enabledBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: Theme.of(context)
                                                            .primaryColorDark)),
                                                focusedErrorBorder:
                                                    UnderlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark)),
                                                focusedBorder:
                                                    UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .primaryColorDark),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          color: Theme.of(context).primaryColor,
                          width: double.infinity,
                          padding: const EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                            top: 20,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 0,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Icon(
                                          Icons.location_on,
                                          size: 22,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            left: 10,
                                          ),
                                          child: Text(
                                            "Pre-order Available?",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        CupertinoSwitch(
                                          onChanged: (bool data) async {
                                            setState(() {
                                              this.enableSwitch =
                                                  this.enableSwitch
                                                      ? false
                                                      : true;
                                            });
                                          },
                                          value: enableSwitch,
                                          activeColor:
                                              Theme.of(context).accentColor,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 5,
                                  left: 32,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      "It is the best place for you to find and order delicious food made",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontSize: 14,
                                        height: 1.4,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w400,
                                        color: Theme.of(context).buttonColor,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                            top: 20,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 10,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Icon(
                                          Icons.bike_scooter,
                                          size: 22,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            left: 10,
                                          ),
                                          child: Text(
                                            "Delivery Options",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    TextFormField(
                                      focusNode: _emailsssFocusdNode,
                                      autofocus: false,
                                      autocorrect: false,
                                      enableInteractiveSelection: true,
                                      onTap: () {
                                        setState(
                                          () {
                                            _emailsssFocusdNode.requestFocus();
                                          },
                                        );
                                      },
                                      style: new TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                      ),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.only(
                                          top: 0,
                                          left: 0,
                                          right: 0,
                                          bottom: 0,
                                        ),
                                        labelText: "Approx Delivery Timing",
                                        labelStyle: TextStyle(
                                          fontFamily: 'Poppins',
                                          color: _emailsssFocusdNode.hasFocus
                                              ? Theme.of(context).accentColor
                                              : Theme.of(context)
                                                  .primaryColorDark,
                                          fontSize: 16,
                                        ),
                                        border: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        disabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        errorBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        focusedErrorBorder:
                                            UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Theme.of(context)
                                                        .primaryColorDark)),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Theme.of(context)
                                                  .primaryColorDark),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    TextFormField(
                                      focusNode: _emailsdddddssFocusdNode,
                                      autofocus: false,
                                      autocorrect: false,
                                      enableInteractiveSelection: true,
                                      onTap: () {
                                        setState(
                                          () {
                                            _emailsdddddssFocusdNode
                                                .requestFocus();
                                          },
                                        );
                                      },
                                      style: new TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                      ),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.only(
                                          top: 0,
                                          left: 0,
                                          right: 0,
                                          bottom: 0,
                                        ),
                                        labelText:
                                            "Minimum Order Value (In Rupee)",
                                        labelStyle: TextStyle(
                                          fontFamily: 'Poppins',
                                          color: _emailsdddddssFocusdNode
                                                  .hasFocus
                                              ? Theme.of(context).accentColor
                                              : Theme.of(context)
                                                  .primaryColorDark,
                                          fontSize: 16,
                                        ),
                                        border: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        disabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        errorBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        focusedErrorBorder:
                                            UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Theme.of(context)
                                                        .primaryColorDark)),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Theme.of(context)
                                                  .primaryColorDark),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    TextFormField(
                                      focusNode: _emailsdddddsssadasdFocusdNode,
                                      autofocus: false,
                                      autocorrect: false,
                                      enableInteractiveSelection: true,
                                      onTap: () {
                                        setState(
                                          () {
                                            _emailsdddddsssadasdFocusdNode
                                                .requestFocus();
                                          },
                                        );
                                      },
                                      style: new TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                      ),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.only(
                                          top: 0,
                                          left: 0,
                                          right: 0,
                                          bottom: 0,
                                        ),
                                        labelText: "Delivery Charges",
                                        labelStyle: TextStyle(
                                          fontFamily: 'Poppins',
                                          color: _emailsdddddsssadasdFocusdNode
                                                  .hasFocus
                                              ? Theme.of(context).accentColor
                                              : Theme.of(context)
                                                  .primaryColorDark,
                                          fontSize: 16,
                                        ),
                                        border: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        disabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        errorBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark)),
                                        focusedErrorBorder:
                                            UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Theme.of(context)
                                                        .primaryColorDark)),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Theme.of(context)
                                                  .primaryColorDark),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                color: Theme.of(context).primaryColor,
                child: Column(
                  children: [
                    Column(
                      children: [
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 15,
                                bottom: 15,
                                left: 20,
                                right: 20,
                              ),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Icon(
                                        Icons.card_giftcard,
                                        size: 22,
                                        color:
                                            Theme.of(context).primaryColorDark,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          left: 10,
                                        ),
                                        child: Text(
                                          "Payment Methods",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 16,
                                            height: 1,
                                            letterSpacing: 0.3,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w500,
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 0,
                            left: 20,
                            right: 20,
                            bottom: 20,
                          ),
                          child: Row(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Center(
                                    child: InkWell(
                                      onTap: () {
                                        setState(() {
                                          _value4 = !_value4;
                                        });
                                      },
                                      child: Container(
                                        width: 25,
                                        height: 25,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          color: Theme.of(context).accentColor,
                                          borderRadius:
                                              BorderRadius.circular(50),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                            left: 0,
                                            top: 0,
                                            right: 0,
                                            bottom: 0,
                                          ),
                                          child: _value4
                                              ? Icon(
                                                  Icons.check,
                                                  size: 19,
                                                  color: Theme.of(context)
                                                      .primaryColorDark,
                                                )
                                              : Icon(
                                                  Icons.check_box_outline_blank,
                                                  size: 19,
                                                  color: Theme.of(context)
                                                      .accentColor,
                                                ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      left: 12,
                                    ),
                                    child: Text(
                                      "Online",
                                      style: TextStyle(
                                        fontSize: 16,
                                        height: 1,
                                        letterSpacing: 0.3,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w600,
                                        color:
                                            Theme.of(context).primaryColorDark,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 10,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Center(
                                      child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            _value3 = !_value3;
                                          });
                                        },
                                        child: Container(
                                          width: 25,
                                          height: 25,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.rectangle,
                                            color:
                                                Theme.of(context).accentColor,
                                            borderRadius:
                                                BorderRadius.circular(50),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                              left: 0,
                                              top: 0,
                                              right: 0,
                                              bottom: 0,
                                            ),
                                            child: _value3
                                                ? Icon(
                                                    Icons.check,
                                                    size: 19,
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                  )
                                                : Icon(
                                                    Icons
                                                        .check_box_outline_blank,
                                                    size: 19,
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                  ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 12,
                                      ),
                                      child: Text(
                                        "Cash On Delivery",
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w600,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: double.infinity,
                      height: 55,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) {
                              return states.contains(MaterialState.pressed)
                                  ? Theme.of(context).accentColor
                                  : Theme.of(context).accentColor;
                            },
                          ),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(0),
                              side: BorderSide(
                                color: Theme.of(context).accentColor,
                              ),
                            ),
                          ),
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => AddMenuScreen(),
                            ),
                          );
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Create Restro Info",
                              style: TextStyle(
                                color: Theme.of(context).primaryColorDark,
                                fontWeight: FontWeight.w600,
                                fontFamily: 'Poppins',
                                letterSpacing: 0.5,
                                fontSize: 16,
                                height: 1,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
