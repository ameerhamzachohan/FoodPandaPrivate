import 'package:flutter/material.dart';
import 'package:foodpanda/screens/private/service_provider/private/accounts_screen.dart';
import 'package:foodpanda/screens/private/service_provider/private/items_screen.dart';
import 'package:foodpanda/screens/private/service_provider/private/order_screen.dart';

// ignore: must_be_immutable
class MainDashboardScreen extends StatefulWidget {
  var pageIndex;
  // int tabViewIndex;
  MainDashboardScreen(this.pageIndex);
  @override
  _MainDashboardScreenState createState() => _MainDashboardScreenState();
}

class _MainDashboardScreenState extends State<MainDashboardScreen>
    with SingleTickerProviderStateMixin {
  var tabIndex = 0;
  @override
  void initState() {
    super.initState();
    _selectedIndex = widget.pageIndex;
  }

  var newNotification;
  var newMessage;
  int _selectedIndex = 0;

  @override
  void didChangeDependencies() {
    setState(() {
      _selectedIndex = widget.pageIndex;
    });

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Theme.of(context).primaryColorDark,
            size: 18,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Theme.of(context).primaryColor,
              size: 18,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
        title: Center(
          child: Text(
            _selectedIndex == 0
                ? 'New Orders'
                : _selectedIndex == 1
                    ? 'Items'
                    : _selectedIndex == 2
                        ? 'Accounts'
                        : '',
            style: TextStyle(
              color: Theme.of(context).primaryColorDark,
              fontWeight: FontWeight.w600,
              fontSize: 16,
              fontFamily: 'Poppins',
              letterSpacing: 0.3,
              height: 1,
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        child: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Theme.of(context).primaryColor,
          ),
          child: BottomNavigationBar(
            backgroundColor: Theme.of(context).primaryColor,
            unselectedItemColor: Theme.of(context).buttonColor,
            selectedIconTheme: IconThemeData(
              color: Theme.of(context).accentColor,
            ),
            selectedItemColor: Theme.of(context).accentColor,
            unselectedIconTheme: IconThemeData(
              color: Theme.of(context).buttonColor,
            ),
            showSelectedLabels: true,
            showUnselectedLabels: true,
            iconSize: 80,
            elevation: 0,
            type: BottomNavigationBarType.fixed,
            currentIndex: _selectedIndex,
            onTap: (int index) {
              setState(() {
                _selectedIndex = index;
              });
              // _navigateToScreens(index);
            },
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.list,
                  size: 22,
                ),
                label: "Orders",
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.workspaces,
                  size: 22,
                ),
                label: "Items",
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.person,
                  size: 22,
                ),
                label: "Account",
              ),
            ],
          ),
        ),
      ),
      body: IndexedStack(
        index: _selectedIndex,
        children: [
          OrderScreen(),
          ItemScreen(),
          AccountsScreen(),
        ],
      ),
    );
  }
}
