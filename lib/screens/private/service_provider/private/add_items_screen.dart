import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddItemScreen extends StatefulWidget {
  @override
  _AddItemScreenState createState() => _AddItemScreenState();
}

class _AddItemScreenState extends State<AddItemScreen> {
  bool enableSwitch = true;
  bool _value = true;
  bool _value2 = true;
  FocusNode _fullNameFocusNode = new FocusNode();
  FocusNode _phoneFocusNode = new FocusNode();
  FocusNode _emailFocusNode = new FocusNode();
  FocusNode _passwordFocusNode = new FocusNode();
  FocusNode _confirmPasswordFocusNode = new FocusNode();
  FocusNode _confirmPassworddFocusNode = new FocusNode();
  FocusNode _confirmPasswordddFocusNode = new FocusNode();

  @override
  void initState() {
    super.initState();
    _fullNameFocusNode = FocusNode();
    _phoneFocusNode = FocusNode();
    _emailFocusNode = FocusNode();
    _passwordFocusNode = FocusNode();
    _confirmPasswordFocusNode = FocusNode();
    _confirmPassworddFocusNode = FocusNode();
    _confirmPasswordddFocusNode = FocusNode();
  }

  @override
  void dispose() {
    _fullNameFocusNode.dispose();
    _phoneFocusNode.dispose();
    _emailFocusNode.dispose();
    _passwordFocusNode.dispose();
    _confirmPasswordFocusNode.dispose();
    _confirmPassworddFocusNode.dispose();
    _confirmPasswordddFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
        return Future.value(false);
      },
      child: Scaffold(
        body: DefaultTabController(
          length: 2,
          child: NestedScrollView(
            headerSliverBuilder: (
              BuildContext context,
              bool innerBoxIsScrolled,
            ) {
              return [
                SliverAppBar(
                  leading: IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Theme.of(context).accentColor,
                      size: 18,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  actions: [],
                  expandedHeight: 350,
                  floating: false,
                  pinned: true,
                  backgroundColor: Theme.of(context).primaryColor,
                  flexibleSpace: FlexibleSpaceBar(
                    background: Container(
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            padding: const EdgeInsets.only(
                              top: 20,
                              left: 20,
                              right: 20,
                              bottom: 20,
                            ),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(50),
                                topLeft: Radius.circular(50),
                                bottomRight: Radius.circular(50),
                                bottomLeft: Radius.circular(50),
                              ),
                              color: Theme.of(context).cardColor,
                            ),
                            child: Icon(
                              Icons.camera_alt_rounded,
                              color: Theme.of(context).accentColor,
                              size: 35,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25,
                            ),
                            child: Text(
                              "Upload item image",
                              style: TextStyle(
                                fontSize: 22,
                                height: 1,
                                letterSpacing: 0.3,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w400,
                                color: Theme.of(context).primaryColorDark,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 10,
                            ),
                            child: Text(
                              "(Ratio Should Be 1:1)",
                              style: TextStyle(
                                fontSize: 16,
                                height: 1,
                                letterSpacing: 0.3,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w400,
                                color: Theme.of(context).buttonColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ];
            },
            body: Container(
              color: Theme.of(context).cardColor,
              width: double.infinity,
              height: double.infinity,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                        bottom: 30,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                              top: 15,
                            ),
                            child: TextFormField(
                              focusNode: _phoneFocusNode,
                              autofocus: false,
                              autocorrect: false,
                              enableInteractiveSelection: true,
                              onTap: () {
                                setState(() {
                                  _phoneFocusNode.requestFocus();
                                });
                              },
                              style: new TextStyle(
                                color: Theme.of(context).primaryColorDark,
                                fontSize: 18,
                                fontFamily: 'Poppins',
                              ),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(
                                  top: 0,
                                  left: 0,
                                  right: 0,
                                  bottom: 0,
                                ),
                                labelText: "Item Name",
                                labelStyle: TextStyle(
                                  fontFamily: 'Poppins',
                                  color: _phoneFocusNode.hasFocus
                                      ? Theme.of(context).accentColor
                                      : Theme.of(context).primaryColorDark,
                                  fontSize: 16,
                                ),
                                border: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .primaryColorDark)),
                                disabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .primaryColorDark)),
                                errorBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .primaryColorDark)),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .primaryColorDark)),
                                focusedErrorBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .primaryColorDark)),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Theme.of(context).primaryColorDark),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 15,
                            ),
                            child: TextFormField(
                              focusNode: _emailFocusNode,
                              autofocus: false,
                              autocorrect: false,
                              enableInteractiveSelection: true,
                              onTap: () {
                                setState(() {
                                  _emailFocusNode.requestFocus();
                                });
                              },
                              style: new TextStyle(
                                color: Theme.of(context).primaryColorDark,
                                fontSize: 18,
                                fontFamily: 'Poppins',
                              ),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(
                                  top: 0,
                                  left: 0,
                                  right: 0,
                                  bottom: 0,
                                ),
                                labelText: "Choose Item Category",
                                labelStyle: TextStyle(
                                  fontFamily: 'Poppins',
                                  color: _emailFocusNode.hasFocus
                                      ? Theme.of(context).accentColor
                                      : Theme.of(context).primaryColorDark,
                                  fontSize: 16,
                                ),
                                border: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .primaryColorDark)),
                                disabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .primaryColorDark)),
                                errorBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .primaryColorDark)),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .primaryColorDark)),
                                focusedErrorBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .primaryColorDark)),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Theme.of(context).primaryColorDark),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 15,
                            ),
                            child: TextFormField(
                              focusNode: _fullNameFocusNode,
                              autofocus: false,
                              autocorrect: false,
                              enableInteractiveSelection: true,
                              onTap: () {
                                setState(() {
                                  _fullNameFocusNode.requestFocus();
                                });
                              },
                              style: new TextStyle(
                                color: Theme.of(context).primaryColorDark,
                                fontSize: 18,
                                fontFamily: 'Poppins',
                              ),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(
                                  top: 0,
                                  left: 0,
                                  right: 0,
                                  bottom: 0,
                                ),
                                labelText: "Short Descriptions",
                                labelStyle: TextStyle(
                                  fontFamily: 'Poppins',
                                  color: _fullNameFocusNode.hasFocus
                                      ? Theme.of(context).accentColor
                                      : Theme.of(context).primaryColorDark,
                                  fontSize: 16,
                                ),
                                border: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .primaryColorDark)),
                                disabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .primaryColorDark)),
                                errorBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .primaryColorDark)),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .primaryColorDark)),
                                focusedErrorBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .primaryColorDark)),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Theme.of(context).primaryColorDark),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 15,
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      width: 200,
                                      child: TextFormField(
                                        focusNode: _passwordFocusNode,
                                        autofocus: false,
                                        autocorrect: false,
                                        enableInteractiveSelection: true,
                                        onTap: () {
                                          setState(() {
                                            _passwordFocusNode.requestFocus();
                                          });
                                        },
                                        style: new TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                          fontSize: 18,
                                          fontFamily: 'Poppins',
                                        ),
                                        decoration: InputDecoration(
                                          labelText: "Price",
                                          contentPadding: EdgeInsets.only(
                                            top: 0,
                                            left: 0,
                                            right: 0,
                                            bottom: 0,
                                          ),
                                          labelStyle: TextStyle(
                                            fontFamily: 'Poppins',
                                            color: _passwordFocusNode.hasFocus
                                                ? Theme.of(context).accentColor
                                                : Theme.of(context)
                                                    .primaryColorDark,
                                            fontSize: 16,
                                          ),
                                          border: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Theme.of(context)
                                                      .primaryColorDark)),
                                          disabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Theme.of(context)
                                                      .primaryColorDark)),
                                          errorBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Theme.of(context)
                                                      .primaryColorDark)),
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Theme.of(context)
                                                      .primaryColorDark)),
                                          focusedErrorBorder:
                                              UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .primaryColorDark)),
                                          focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColorDark),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 0,
                                      ),
                                      child: Text(
                                        "Available?",
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context).buttonColor,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 10,
                                      ),
                                      child: SizedBox(
                                        height: 20,
                                        child: CupertinoSwitch(
                                          onChanged: (bool data) async {
                                            setState(
                                              () {
                                                this.enableSwitch =
                                                    this.enableSwitch
                                                        ? false
                                                        : true;
                                              },
                                            );
                                          },
                                          value: enableSwitch,
                                          activeColor:
                                              Theme.of(context).accentColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 12,
                      color: Theme.of(context).primaryColor,
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                        bottom: 30,
                        top: 30,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Text(
                                  "Add Specification (Optional)",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontSize: 20,
                                    height: 1,
                                    letterSpacing: 0.3,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w400,
                                    color: Theme.of(context).primaryColorDark,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                  top: 15,
                                ),
                                child: TextFormField(
                                  focusNode: _confirmPasswordFocusNode,
                                  autofocus: false,
                                  autocorrect: false,
                                  enableInteractiveSelection: true,
                                  onTap: () {
                                    setState(() {
                                      _confirmPasswordFocusNode.requestFocus();
                                    });
                                  },
                                  style: new TextStyle(
                                    color: Theme.of(context).primaryColorDark,
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                  ),
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                      top: 0,
                                      left: 0,
                                      right: 0,
                                      bottom: 0,
                                    ),
                                    labelText:
                                        "Add Title (i.e Extra toppings, size, etc.)",
                                    labelStyle: TextStyle(
                                      fontFamily: 'Poppins',
                                      color: _confirmPasswordFocusNode.hasFocus
                                          ? Theme.of(context).accentColor
                                          : Theme.of(context).primaryColorDark,
                                      fontSize: 16,
                                    ),
                                    border: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Theme.of(context)
                                                .primaryColorDark)),
                                    disabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Theme.of(context)
                                                .primaryColorDark)),
                                    errorBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Theme.of(context)
                                                .primaryColorDark)),
                                    enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Theme.of(context)
                                                .primaryColorDark)),
                                    focusedErrorBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Theme.of(context)
                                                .primaryColorDark)),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .primaryColorDark),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          width: 200,
                                          child: TextFormField(
                                            focusNode:
                                                _confirmPassworddFocusNode,
                                            autofocus: false,
                                            autocorrect: false,
                                            enableInteractiveSelection: true,
                                            onTap: () {
                                              setState(() {
                                                _confirmPassworddFocusNode
                                                    .requestFocus();
                                              });
                                            },
                                            style: new TextStyle(
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                              fontSize: 18,
                                              fontFamily: 'Poppins',
                                            ),
                                            decoration: InputDecoration(
                                              labelText: "Option 1",
                                              contentPadding: EdgeInsets.only(
                                                top: 0,
                                                left: 0,
                                                right: 0,
                                                bottom: 0,
                                              ),
                                              labelStyle: TextStyle(
                                                fontFamily: 'Poppins',
                                                color: _confirmPassworddFocusNode
                                                        .hasFocus
                                                    ? Theme.of(context)
                                                        .accentColor
                                                    : Theme.of(context)
                                                        .primaryColorDark,
                                                fontSize: 16,
                                              ),
                                              border: UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .primaryColorDark)),
                                              disabledBorder:
                                                  UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColorDark)),
                                              errorBorder: UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .primaryColorDark)),
                                              enabledBorder: UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .primaryColorDark)),
                                              focusedErrorBorder:
                                                  UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColorDark)),
                                              focusedBorder:
                                                  UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          width: 120,
                                          child: TextFormField(
                                            focusNode:
                                                _confirmPasswordddFocusNode,
                                            autofocus: false,
                                            autocorrect: false,
                                            enableInteractiveSelection: true,
                                            onTap: () {
                                              setState(() {
                                                _confirmPasswordddFocusNode
                                                    .requestFocus();
                                              });
                                            },
                                            style: new TextStyle(
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                              fontSize: 18,
                                              fontFamily: 'Poppins',
                                            ),
                                            decoration: InputDecoration(
                                              labelText: "Price",
                                              contentPadding: EdgeInsets.only(
                                                top: 0,
                                                left: 0,
                                                right: 0,
                                                bottom: 0,
                                              ),
                                              labelStyle: TextStyle(
                                                fontFamily: 'Poppins',
                                                color:
                                                    _confirmPasswordddFocusNode
                                                            .hasFocus
                                                        ? Theme.of(context)
                                                            .accentColor
                                                        : Theme.of(context)
                                                            .primaryColorDark,
                                                fontSize: 16,
                                              ),
                                              border: UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .primaryColorDark)),
                                              disabledBorder:
                                                  UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColorDark)),
                                              errorBorder: UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .primaryColorDark)),
                                              enabledBorder: UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .primaryColorDark)),
                                              focusedErrorBorder:
                                                  UnderlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColorDark)),
                                              focusedBorder:
                                                  UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 25,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.add,
                                      color: Theme.of(context).accentColor,
                                      size: 22,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 10,
                                      ),
                                      child: Text(
                                        "More Options",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 15,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context).accentColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 40,
                                ),
                                child: Text(
                                  "Customer Can Choose",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontSize: 18,
                                    height: 1,
                                    letterSpacing: 0.3,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w400,
                                    color: Theme.of(context).primaryColorDark,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 20,
                                  bottom: 0,
                                  left: 0,
                                  right: 0,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Center(
                                          child: InkWell(
                                            onTap: () {
                                              setState(
                                                () {
                                                  _value = !_value;
                                                },
                                              );
                                            },
                                            child: Container(
                                              width: 25,
                                              height: 25,
                                              padding: const EdgeInsets.only(
                                                left: 0,
                                                top: 0,
                                                right: 0,
                                                bottom: 0,
                                              ),
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.rectangle,
                                                  color: Theme.of(context).hintColor,
                                                  borderRadius:
                                                      BorderRadius.circular(50),
                                                  border: Border.all(
                                                      color: Theme.of(context)
                                                          .buttonColor)),
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                  left: 0,
                                                  top: 0,
                                                  right: 0,
                                                  bottom: 0,
                                                ),
                                                child: _value
                                                    ? Icon(
                                                        Icons.check,
                                                        size: 18,
                                                        color: Theme.of(context)
                                                            .primaryColorDark,
                                                      )
                                                    : Icon(
                                                        Icons
                                                            .check_box_outline_blank,
                                                        size: 18,
                                                        color: Theme.of(context)
                                                            .cardColor,
                                                      ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            left: 12,
                                          ),
                                          child: Text(
                                            "Only One Options",
                                            style: TextStyle(
                                              fontSize: 13,
                                              height: 1,
                                              letterSpacing: 0.3,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 20,
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Center(
                                            child: InkWell(
                                              onTap: () {
                                                setState(
                                                  () {
                                                    _value2 = !_value2;
                                                  },
                                                );
                                              },
                                              child: Container(
                                                width: 25,
                                                height: 25,
                                                padding: const EdgeInsets.only(
                                                  left: 0,
                                                  top: 0,
                                                  right: 0,
                                                  bottom: 0,
                                                ),
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.rectangle,
                                                    color: Theme.of(context).hintColor,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            50),
                                                    border: Border.all(
                                                        color: Theme.of(context)
                                                            .buttonColor)),
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                    left: 0,
                                                    top: 0,
                                                    right: 0,
                                                    bottom: 0,
                                                  ),
                                                  child: _value2
                                                      ? Icon(
                                                          Icons.check,
                                                          size: 18,
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColorDark,
                                                        )
                                                      : Icon(
                                                          Icons
                                                              .check_box_outline_blank,
                                                          size: 18,
                                                          color:
                                                              Theme.of(context)
                                                                  .cardColor,
                                                        ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              left: 12,
                                            ),
                                            child: Text(
                                              "Multiple Options",
                                              style: TextStyle(
                                                fontSize: 13,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        bottom: 30,
                      ),
                      child: Container(
                        color: Theme.of(context).primaryColor,
                        child: Padding(
                          padding: const EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: 20,
                            top: 20,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Icon(
                                Icons.add,
                                color: Theme.of(context).accentColor,
                                size: 22,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 10,
                                ),
                                child: Text(
                                  "Add More Specifications",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontSize: 15,
                                    height: 1,
                                    letterSpacing: 0.3,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w400,
                                    color: Theme.of(context).accentColor,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        bottomNavigationBar: Container(
          color: Theme.of(context).cardColor,
          padding: EdgeInsets.only(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
          ),
          child: SizedBox(
            width: double.infinity,
            height: 55,
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.resolveWith<Color>(
                  (Set<MaterialState> states) {
                    return states.contains(MaterialState.pressed)
                        ? Theme.of(context).accentColor
                        : Theme.of(context).accentColor;
                  },
                ),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0),
                    side: BorderSide(
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                ),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Update Item",
                    style: TextStyle(
                      color: Theme.of(context).primaryColorDark,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Poppins',
                      letterSpacing: 0.5,
                      fontSize: 16,
                      height: 1,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
