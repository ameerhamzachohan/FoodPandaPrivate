import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AdminScreen extends StatefulWidget {
  @override
  _AdminScreenState createState() => _AdminScreenState();
}

class _AdminScreenState extends State<AdminScreen> {
  bool isChecked = true;
  bool _value = true;
  bool _value2 = true;
  bool _value3 = true;
  bool _value4 = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: SafeArea(
        top: true,
        bottom: true,
        child: Container(
          color: Theme.of(context).cardColor,
          width: double.infinity,
          height: double.infinity,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: 8,
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      color: Theme.of(context).primaryColorDark,
                      padding: const EdgeInsets.only(
                        left: 15,
                        right: 15,
                      ),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Flexible(
                                flex: 2,
                                child: Center(
                                  child: Container(
                                    width: double.infinity,
                                    height: 100,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(6),
                                      image: DecorationImage(
                                          image: AssetImage(
                                              "assets/images/logo-mcdonalds.jpg"),
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 5,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    top: 15,
                                    left: 15,
                                    bottom: 15,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                Text(
                                                  "Marshall John",
                                                  style: TextStyle(
                                                    fontSize: 16,
                                                    height: 1,
                                                    letterSpacing: 0.3,
                                                    fontFamily: 'Poppins',
                                                    fontWeight: FontWeight.w600,
                                                    color: Theme.of(context)
                                                        .buttonColor,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                    left: 10,
                                                    right: 0,
                                                  ),
                                                  child: Image.asset(
                                                    "assets/images/icon-call.png",
                                                    width: 35,
                                                    height: 35,
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                    left: 5,
                                                    right: 0,
                                                  ),
                                                  child: Image.asset(
                                                    "assets/images/icon-mesage.png",
                                                    width: 35,
                                                    height: 35,
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                    left: 5,
                                                    right: 0,
                                                  ),
                                                  child: Image.asset(
                                                    "assets/images/icon-setting.png",
                                                    width: 35,
                                                    height: 35,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 5,
                                        ),
                                        child: Container(
                                          width: 220,
                                          child: Text(
                                            "marshall@gmail.com",
                                            style: TextStyle(
                                              fontSize: 15,
                                              letterSpacing: 0.3,
                                              height: 1,
                                              color:
                                                  Theme.of(context).buttonColor,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                            ),
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Container(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Text(
                                                "Date:",
                                                style: TextStyle(
                                                  fontSize: 15,
                                                  letterSpacing: 0.3,
                                                  height: 1,
                                                  color: Theme.of(context)
                                                      .buttonColor,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                              Text(
                                                "4th October 2020",
                                                style: TextStyle(
                                                  fontSize: 15,
                                                  letterSpacing: 0.3,
                                                  height: 1,
                                                  color: Theme.of(context)
                                                      .buttonColor,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 5,
                                        ),
                                        child: Container(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Text(
                                                "Order ID:",
                                                style: TextStyle(
                                                  fontSize: 15,
                                                  letterSpacing: 0.3,
                                                  height: 1,
                                                  color: Theme.of(context)
                                                      .buttonColor,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                              Text(
                                                "xWh4dSa",
                                                style: TextStyle(
                                                  fontSize: 15,
                                                  letterSpacing: 0.3,
                                                  height: 1,
                                                  color: Theme.of(context)
                                                      .buttonColor,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                          Container(
                            color: Theme.of(context).primaryColorDark,
                            child: Padding(
                              padding: EdgeInsets.only(
                                top: 0,
                                bottom: 15,
                              ),
                              child: Divider(
                                height: 1,
                                thickness: 1,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 0,
                              bottom: 0,
                            ),
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(
                                    top: 0,
                                    bottom: 15,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "Item",
                                              style: TextStyle(
                                                fontSize: 16,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w600,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "Quantity",
                                              style: TextStyle(
                                                fontSize: 16,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w600,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "Price",
                                              style: TextStyle(
                                                fontSize: 16,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w600,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                    top: 0,
                                    bottom: 15,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "Briyani",
                                              style: TextStyle(
                                                fontSize: 15,
                                                letterSpacing: 0.3,
                                                height: 1,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "1",
                                              style: TextStyle(
                                                fontSize: 15,
                                                letterSpacing: 0.3,
                                                height: 1,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "130.00 Rs",
                                              style: TextStyle(
                                                fontSize: 15,
                                                letterSpacing: 0.3,
                                                height: 1,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                    top: 0,
                                    bottom: 15,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "Briyani",
                                              style: TextStyle(
                                                fontSize: 15,
                                                letterSpacing: 0.3,
                                                height: 1,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "1",
                                              style: TextStyle(
                                                fontSize: 15,
                                                letterSpacing: 0.3,
                                                height: 1,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "130.00 Rs",
                                              style: TextStyle(
                                                fontSize: 15,
                                                letterSpacing: 0.3,
                                                height: 1,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                    top: 0,
                                    bottom: 15,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "Total Price",
                                              style: TextStyle(
                                                fontSize: 16,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w600,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "",
                                              style: TextStyle(
                                                fontSize: 16,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w600,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "320.00 Rs",
                                              style: TextStyle(
                                                fontSize: 16,
                                                height: 1,
                                                letterSpacing: 0.3,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w600,
                                                color: Theme.of(context)
                                                    .buttonColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            color: Theme.of(context).primaryColorDark,
                            child: Padding(
                              padding: EdgeInsets.only(
                                top: 0,
                                bottom: 15,
                              ),
                              child: Divider(
                                height: 1,
                                thickness: 1,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 0,
                              bottom: 15,
                            ),
                            child: Row(
                              children: [
                                Text(
                                  "Order Status",
                                  style: TextStyle(
                                    fontSize: 16,
                                    height: 1,
                                    letterSpacing: 0.3,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600,
                                    color: Theme.of(context).buttonColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 0,
                                  bottom: 20,
                                  left: 0,
                                  right: 0,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Center(
                                      child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            _value = !_value;
                                          });
                                        },
                                        child: Container(
                                          width: 35,
                                          height: 35,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.rectangle,
                                            color:
                                                Theme.of(context).accentColor,
                                            borderRadius:
                                                BorderRadius.circular(50),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                              left: 0,
                                              top: 0,
                                              right: 0,
                                              bottom: 0,
                                            ),
                                            child: _value
                                                ? Icon(
                                                    Icons.check,
                                                    size: 22,
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                  )
                                                : Icon(
                                                    Icons
                                                        .check_box_outline_blank,
                                                    size: 22,
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                  ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 12,
                                      ),
                                      child: Text(
                                        "Order Placed",
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w600,
                                          color: Theme.of(context).buttonColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 0,
                                  left: 0,
                                  right: 0,
                                  bottom: 20,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Center(
                                      child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            _value2 = !_value2;
                                          });
                                        },
                                        child: Container(
                                          width: 35,
                                          height: 35,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.rectangle,
                                            color:
                                                Theme.of(context).accentColor,
                                            borderRadius:
                                                BorderRadius.circular(50),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                              left: 0,
                                              top: 0,
                                              right: 0,
                                              bottom: 0,
                                            ),
                                            child: _value2
                                                ? Icon(
                                                    Icons.check,
                                                    size: 22,
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                  )
                                                : Icon(
                                                    Icons
                                                        .check_box_outline_blank,
                                                    size: 22,
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                  ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 12,
                                      ),
                                      child: Text(
                                        "Order Confirmed",
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w600,
                                          color: Theme.of(context).buttonColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 0,
                                  left: 0,
                                  right: 0,
                                  bottom: 20,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Center(
                                      child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            _value3 = !_value3;
                                          });
                                        },
                                        child: Container(
                                          width: 35,
                                          height: 35,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.rectangle,
                                            color:
                                                Theme.of(context).accentColor,
                                            borderRadius:
                                                BorderRadius.circular(50),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                              left: 0,
                                              top: 0,
                                              right: 0,
                                              bottom: 0,
                                            ),
                                            child: _value3
                                                ? Icon(
                                                    Icons.check,
                                                    size: 22,
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                  )
                                                : Icon(
                                                    Icons
                                                        .check_box_outline_blank,
                                                    size: 22,
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                  ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 12,
                                      ),
                                      child: Text(
                                        "Order On The Way",
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w600,
                                          color: Theme.of(context).buttonColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 0,
                                  left: 0,
                                  bottom: 20,
                                  right: 0,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Center(
                                      child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            _value4 = !_value4;
                                          });
                                        },
                                        child: Container(
                                          width: 35,
                                          height: 35,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.rectangle,
                                            color:
                                                Theme.of(context).accentColor,
                                            borderRadius:
                                                BorderRadius.circular(50),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                              left: 0,
                                              top: 0,
                                              right: 0,
                                              bottom: 0,
                                            ),
                                            child: _value4
                                                ? Icon(
                                                    Icons.check,
                                                    size: 22,
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                  )
                                                : Icon(
                                                    Icons
                                                        .check_box_outline_blank,
                                                    size: 22,
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                  ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 12,
                                      ),
                                      child: Text(
                                        "Order Delivered",
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1,
                                          letterSpacing: 0.3,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w600,
                                          color: Theme.of(context).buttonColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  color: Theme.of(context).primaryColorDark,
                  padding: EdgeInsets.only(
                    top: 15,
                    left: 20,
                    right: 20,
                    bottom: 15,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: double.infinity,
                        height: 45,
                        child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.resolveWith<Color>(
                              (Set<MaterialState> states) {
                                return states.contains(MaterialState.pressed)
                                    ? Theme.of(context).accentColor
                                    : Theme.of(context).accentColor;
                              },
                            ),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                                side: BorderSide(
                                  color: Theme.of(context).accentColor,
                                ),
                              ),
                            ),
                          ),
                          onPressed: () {},
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Cancel Order",
                                style: TextStyle(
                                  color: Theme.of(context).primaryColorDark,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: 'Poppins',
                                  letterSpacing: 0.5,
                                  fontSize: 13,
                                  height: 1,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).primaryColorDark,
      elevation: 0,
      centerTitle: true,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          color: Theme.of(context).primaryColorDark,
          size: 18,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      title: Text(
        "My Orders",
        style: TextStyle(
          color: Theme.of(context).primaryColorDark,
          fontWeight: FontWeight.w600,
          fontSize: 16,
          fontFamily: 'Poppins',
          letterSpacing: 0.3,
          height: 1,
        ),
      ),
    );
  }
}
