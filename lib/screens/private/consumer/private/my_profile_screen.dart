import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool enableSwitch = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: Container(
        color: Theme.of(context).primaryColor,
        width: double.infinity,
        height: double.infinity,
        child: SafeArea(
          top: true,
          bottom: true,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Center(
                                    child: Text(
                                      "CONTACT INFORMATION",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Theme.of(context).primaryColorDark,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 16,
                                        fontFamily: 'Poppins',
                                        letterSpacing: 0,
                                        height: 1,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SingleChildScrollView(
                              child: Container(
                                color: Theme.of(context).cardColor,
                                padding: const EdgeInsets.all(20),
                                child: Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "First Name",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                        Text(
                                          "Ameer Hamza",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      color: Theme.of(context).cardColor,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          top: 15,
                                          bottom: 15,
                                        ),
                                        child: Divider(
                                          height: 1,
                                          thickness: 1,
                                          color: Theme.of(context).primaryColorDark,
                                        ),
                                      ),
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Last Name",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                        Text(
                                          "Chohan",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      color: Theme.of(context).cardColor,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          top: 15,
                                          bottom: 15,
                                        ),
                                        child: Divider(
                                          height: 1,
                                          color: Theme.of(context).primaryColorDark,
                                          thickness: 1,
                                        ),
                                      ),
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Email Address",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                        Text(
                                          "hameez.raj@hotmail.com",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      color: Theme.of(context).cardColor,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          top: 15,
                                          bottom: 15,
                                        ),
                                        child: Divider(
                                          height: 1,
                                          color: Theme.of(context).primaryColorDark,
                                          thickness: 1,
                                        ),
                                      ),
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Mobile Number",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                        Text(
                                          "+923242492566",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Center(
                                    child: Text(
                                      "SETTINGS",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Theme.of(context).primaryColorDark,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 16,
                                        fontFamily: 'Poppins',
                                        letterSpacing: 0,
                                        height: 1,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SingleChildScrollView(
                              child: Container(
                                color: Theme.of(context).cardColor,
                                padding: const EdgeInsets.all(20),
                                child: Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Language",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                        Text(
                                          "English",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      color: Theme.of(context).cardColor,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          top: 15,
                                          bottom: 15,
                                        ),
                                        child: Divider(
                                          height: 1,
                                          color: Theme.of(context).primaryColorDark,
                                          thickness: 1,
                                        ),
                                      ),
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Change Password",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                        Text(
                                          "******",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      color: Theme.of(context).cardColor,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          top: 15,
                                          bottom: 15,
                                        ),
                                        child: Divider(
                                          height: 1,
                                          color: Theme.of(context).primaryColorDark,
                                          thickness: 1,
                                        ),
                                      ),
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Recive Push Notifications",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                        CupertinoSwitch(
                                          onChanged: (bool data) async {
                                            setState(() {
                                              this.enableSwitch =
                                                  this.enableSwitch
                                                      ? false
                                                      : true;
                                            });
                                          },
                                          value: enableSwitch,
                                          activeColor: Theme.of(context).accentColor,
                                        ),
                                      ],
                                    ),
                                    Container(
                                      color: Theme.of(context).cardColor,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          top: 15,
                                          bottom: 15,
                                        ),
                                        child: Divider(
                                          height: 1,
                                          color: Theme.of(context).primaryColorDark,
                                          thickness: 1,
                                        ),
                                      ),
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Recive Offers By Email",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                        CupertinoSwitch(
                                          onChanged: (bool data) async {
                                            setState(() {
                                              this.enableSwitch =
                                                  this.enableSwitch
                                                      ? false
                                                      : true;
                                            });
                                          },
                                          value: enableSwitch,
                                          activeColor: Theme.of(context).accentColor,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(20),
                              child: SizedBox(
                                width: double.infinity,
                                height: 45,
                                child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty
                                        .resolveWith<Color>(
                                      (Set<MaterialState> states) {
                                        return states
                                                .contains(MaterialState.pressed)
                                            ? Theme.of(context).accentColor
                                            : Theme.of(context).accentColor;
                                      },
                                    ),
                                    shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(0),
                                        side: BorderSide(
                                          color: Theme.of(context).accentColor,
                                        ),
                                      ),
                                    ),
                                  ),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "SAVE",
                                        style: TextStyle(
                                          color: Theme.of(context).primaryColorDark,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: 'Poppins',
                                          letterSpacing: 0.5,
                                          fontSize: 13,
                                          height: 1,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).primaryColor,
      elevation: 0,
      centerTitle: true,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          color: Theme.of(context).primaryColorDark,
          size: 18,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      title: Text(
        "Profile",
        style: TextStyle(
          color: Theme.of(context).primaryColorDark,
          fontWeight: FontWeight.w600,
          fontSize: 16,
          fontFamily: 'Poppins',
          letterSpacing: 0.3,
          height: 1,
        ),
      ),
    );
  }
}
