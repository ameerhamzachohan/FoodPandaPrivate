import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodpanda/screens/private/consumer/private/food_listing_screen.dart';


class CheckoutScreen extends StatefulWidget {
  @override
  _CheckoutScreenState createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen> {
  bool enableSwitch = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: Container(
        color: Theme.of(context).primaryColor,
        width: double.infinity,
        height: double.infinity,
        child: SafeArea(
          top: true,
          bottom: true,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.only(
                        top: 15,
                        bottom: 15,
                        left: 15,
                        right: 15,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            color: Theme.of(context).cardColor,
                            padding: EdgeInsets.only(
                              top: 15,
                              bottom: 15,
                              left: 15,
                              right: 15,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Icon(
                                          Icons.location_on,
                                          color: Theme.of(context).accentColor,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            left: 12,
                                          ),
                                          child: Text(
                                            "My Address",
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w600,
                                              fontSize: 14,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0.3,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        // Navigator.push(
                                        //   context,
                                        //   MaterialPageRoute(
                                        //     builder: (context) =>
                                        //         MyAddressScreen(),
                                        //   ),
                                        // );
                                      },
                                      child: Image.asset(
                                        "assets/images/ic-edit-icon.png",
                                        width: 16,
                                        height: 16,
                                        color: Theme.of(context).accentColor,
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                    top: 15,
                                    left: 0,
                                    right: 0,
                                    bottom: 0,
                                  ),
                                  child: Container(
                                    width: double.infinity,
                                    height: 130,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(6),
                                      border: Border.all(color: Theme.of(context).primaryColorDark),
                                      image: DecorationImage(
                                        image: AssetImage(
                                            "assets/images/dummy-map.jpg"),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    bottom: 0,
                                    left: 0,
                                    right: 0,
                                    top: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Home",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w600,
                                            fontSize: 14,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0.3,
                                            height: 1),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    bottom: 0,
                                    left: 0,
                                    right: 0,
                                    top: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "1340 Del Rosa Way, Sparks, NV, 89434",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w600,
                                            fontSize: 14,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0.3,
                                            height: 1),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    bottom: 0,
                                    left: 0,
                                    right: 0,
                                    top: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Karachi",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w600,
                                            fontSize: 14,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0.3,
                                            height: 1),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  color: Theme.of(context).cardColor,
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                      top: 15,
                                      bottom: 15,
                                    ),
                                    child: Divider(
                                      height: 1,
                                      color: Theme.of(context).primaryColorDark,
                                      thickness: 1,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 0,
                                    bottom: 0,
                                    left: 0,
                                    right: 0,
                                  ),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Jahaan",
                                        style: TextStyle(
                                          color: Theme.of(context).primaryColorDark,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 14,
                                          fontFamily: 'Poppins',
                                          letterSpacing: 0.3,
                                          height: 1,
                                        ),
                                      ),
                                      Image.asset(
                                        "assets/images/right-arrow.png",
                                        width: 15,
                                        height: 15,
                                       color: Theme.of(context).accentColor,
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  color: Theme.of(context).cardColor,
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                      top: 15,
                                      bottom: 15,
                                    ),
                                    child: Divider(
                                      height: 1,
                                      color: Theme.of(context).primaryColorDark,
                                      thickness: 1,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 0,
                                    bottom: 0,
                                    left: 0,
                                    right: 0,
                                  ),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Flexible(
                                        fit: FlexFit.tight,
                                        flex: 10,
                                        child: Text(
                                          "Contactless Delivery: Switch To Online Payment For This Options",
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0.3,
                                            height: 1.4,
                                          ),
                                        ),
                                      ),
                                      Flexible(
                                        fit: FlexFit.loose,
                                        flex: 2,
                                        child: CupertinoSwitch(
                                          onChanged: (bool data) async {
                                            setState(() {
                                              this.enableSwitch =
                                                  this.enableSwitch
                                                      ? false
                                                      : true;
                                            });
                                          },
                                          value: enableSwitch,
                                          activeColor: Theme.of(context).accentColor,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 15,
                            ),
                            child: Container(
                              color: Theme.of(context).cardColor,
                              padding: EdgeInsets.only(
                                top: 15,
                                bottom: 15,
                                left: 15,
                                right: 15,
                              ),
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 0,
                                      bottom: 0,
                                      left: 0,
                                      right: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Icon(
                                              Icons.card_giftcard,
                                              size: 22,
                                              color:
                                                  Theme.of(context).accentColor,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                left: 10,
                                              ),
                                              child: Text(
                                                "Payment Method",
                                                style: TextStyle(
                                                  color: Theme.of(context).primaryColorDark,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 14,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  height: 1,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Image.asset(
                                          "assets/images/ic-edit-icon.png",
                                          width: 16,
                                          height: 16,
                                          color: Theme.of(context).accentColor,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      bottom: 0,
                                      left: 0,
                                      right: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Icon(
                                              Icons.money,
                                              color:
                                                  Theme.of(context).accentColor,
                                              size: 22,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                left: 10,
                                              ),
                                              child: Text(
                                                "Cash",
                                                style: TextStyle(
                                                  color: Theme.of(context).primaryColorDark,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 14,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  height: 1,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Text(
                                          "Rs. 383.00",
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0.3,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 15,
                            ),
                            child: Container(
                              color: Theme.of(context).cardColor,
                              padding: EdgeInsets.only(
                                top: 15,
                                bottom: 15,
                                left: 15,
                                right: 15,
                              ),
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 0,
                                      bottom: 0,
                                      left: 0,
                                      right: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Icon(
                                              Icons.card_membership,
                                              size: 22,
                                              color:
                                                  Theme.of(context).accentColor,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                left: 10,
                                              ),
                                              child: Text(
                                                "Order Summary",
                                                style: TextStyle(
                                                  color: Theme.of(context).primaryColorDark,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 14,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  height: 1,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      bottom: 0,
                                      left: 0,
                                      right: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                left: 0,
                                              ),
                                              child: Text(
                                                "1x Food Fest Deal 1",
                                                style: TextStyle(
                                                  color: Theme.of(context).primaryColorDark,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 14,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  height: 1,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Text(
                                          "Rs. 383.00",
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0.3,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    color: Theme.of(context).cardColor,
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                        top: 15,
                                        bottom: 15,
                                      ),
                                      child: Divider(
                                        height: 1,
                                        color: Theme.of(context).primaryColorDark,
                                        thickness: 1,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 0,
                                      bottom: 0,
                                      left: 0,
                                      right: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                left: 0,
                                              ),
                                              child: Text(
                                                "Subtotal",
                                                style: TextStyle(
                                                  color: Theme.of(context).primaryColorDark,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 14,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  height: 1,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Text(
                                          "Rs. 383.00",
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0.3,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 15,
                                      bottom: 0,
                                      left: 0,
                                      right: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                left: 0,
                                              ),
                                              child: Text(
                                                "Delivery Fee",
                                                style: TextStyle(
                                                  color: Theme.of(context).primaryColorDark,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 14,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  height: 1,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Text(
                                          "Rs. 383.00",
                                          style: TextStyle(
                                            color: Theme.of(context).primaryColorDark,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0.3,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 15,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  "By Completing This Order, I Agree To All Terms & Condations",
                                  style: TextStyle(
                                    color: Theme.of(context).primaryColorDark,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16,
                                    fontFamily: 'Poppins',
                                    letterSpacing: 0,
                                    height: 1.4,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  color: Theme.of(context).primaryColor,
                  padding: EdgeInsets.only(
                    top: 15,
                    left: 20,
                    right: 20,
                    bottom: 15,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 10,
                          bottom: 20,
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Total (incl. VAT)",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Theme.of(context).primaryColorDark,
                                fontWeight: FontWeight.w700,
                                fontSize: 16,
                                fontFamily: 'Poppins',
                                letterSpacing: 0,
                                height: 1,
                              ),
                            ),
                            Text(
                              "Rs. 549.00",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Theme.of(context).primaryColorDark,
                                fontWeight: FontWeight.w700,
                                fontSize: 16,
                                fontFamily: 'Poppins',
                                letterSpacing: 0,
                                height: 1,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: double.infinity,
                        height: 45,
                        child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.resolveWith<Color>(
                              (Set<MaterialState> states) {
                                return states.contains(MaterialState.pressed)
                                    ? Theme.of(context).accentColor
                                    : Theme.of(context).accentColor;
                              },
                            ),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                                side: BorderSide(
                                color: Theme.of(context).accentColor,
                                ),
                              ),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => FoodListingScreen(),
                              ),
                            );
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "PLACE ORDER",
                                style: TextStyle(
                                  color: Theme.of(context).primaryColorDark,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: 'Poppins',
                                  letterSpacing: 0.5,
                                  fontSize: 13,
                                  height: 1,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).primaryColor,
      elevation: 0,
      centerTitle: true,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          color: Theme.of(context).primaryColorDark,
          size: 18,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      title: Text(
        "Checkout",
        style: TextStyle(
          color: Theme.of(context).primaryColorDark,
          fontWeight: FontWeight.w600,
          fontSize: 16,
          fontFamily: 'Poppins',
          letterSpacing: 0.3,
          height: 1,
        ),
      ),
    );
  }
}
