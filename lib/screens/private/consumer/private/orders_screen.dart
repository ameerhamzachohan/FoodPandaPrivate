import 'package:flutter/material.dart';
import 'package:foodpanda/screens/private/consumer/private/food_detail_screen.dart';
import 'package:foodpanda/screens/private/consumer/private/orders_status_screen.dart';

class OrdersScreen extends StatefulWidget {
  @override
  _OrdersScreenState createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrdersScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: Container(
        color: Theme.of(context).primaryColor,
        width: double.infinity,
        height: double.infinity,
        child: SafeArea(
          top: true,
          bottom: true,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(20),
                          child: Text(
                            "ON-GOING ORDERS",
                            style: TextStyle(
                              color: Theme.of(context).primaryColorDark,
                              fontWeight: FontWeight.w700,
                              fontSize: 16,
                              fontFamily: 'Poppins',
                              letterSpacing: 0,
                              height: 1,
                            ),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 20,
                            right: 20,
                          ),
                          decoration: BoxDecoration(
                            color: Theme.of(context).cardColor,
                          ),
                          child: Column(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => OrderStatusScreen(),
                                    ),
                                  );
                                },
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(0),
                                          child: Text(
                                            "Broadway Pizza - Malir Cantt",
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 8,
                                          ),
                                          child: Text(
                                            "15 May, 15:58",
                                            style: TextStyle(
                                              color: Theme.of(context).buttonColor,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 8,
                                          ),
                                          child: Text(
                                            "Festive Deal 1",
                                            style: TextStyle(
                                              color: Theme.of(context).buttonColor,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(0),
                                          child: Text(
                                            "Rs. 548.00",
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                color: Theme.of(context).cardColor,
                                child: Padding(
                                  padding: EdgeInsets.only(
                                    top: 15,
                                    bottom: 15,
                                  ),
                                  child: Divider(
                                    height: 1,
                                    color: Theme.of(context).primaryColorDark,
                                    thickness: 1,
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => OrderStatusScreen(),
                                    ),
                                  );
                                },
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(0),
                                          child: Text(
                                            "Broadway Pizza - Malir Cantt",
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 8,
                                          ),
                                          child: Text(
                                            "15 May, 15:58",
                                            style: TextStyle(
                                              color: Theme.of(context).buttonColor,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 8,
                                          ),
                                          child: Text(
                                            "Festive Deal 1",
                                            style: TextStyle(
                                              color: Theme.of(context).buttonColor,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(0),
                                          child: Text(
                                            "Rs. 548.00",
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                color: Theme.of(context).cardColor,
                                child: Padding(
                                  padding: EdgeInsets.only(
                                    top: 15,
                                    bottom: 15,
                                  ),
                                  child: Divider(
                                    height: 1,
                                    color: Theme.of(context).primaryColorDark,
                                    thickness: 1,
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => OrderStatusScreen(),
                                    ),
                                  );
                                },
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(0),
                                          child: Text(
                                            "Broadway Pizza - Malir Cantt",
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 8,
                                          ),
                                          child: Text(
                                            "15 May, 15:58",
                                            style: TextStyle(
                                              color: Theme.of(context).buttonColor,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 8,
                                          ),
                                          child: Text(
                                            "Festive Deal 1",
                                            style: TextStyle(
                                              color: Theme.of(context).buttonColor,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(0),
                                          child: Text(
                                            "Rs. 548.00",
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20),
                          child: Text(
                            "PAST ORDERS",
                            style: TextStyle(
                              color: Theme.of(context).primaryColorDark,
                              fontWeight: FontWeight.w700,
                              fontSize: 16,
                              fontFamily: 'Poppins',
                              letterSpacing: 0,
                              height: 1,
                            ),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 20,
                            right: 20,
                          ),
                          decoration: BoxDecoration(
                            color: Theme.of(context).cardColor,
                          ),
                          child: Column(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => FoodDetailScreen(),
                                    ),
                                  );
                                },
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(0),
                                          child: Text(
                                            "Broadway Pizza - Malir Cantt",
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 8,
                                          ),
                                          child: Text(
                                            "15 May, 15:58",
                                            style: TextStyle(
                                              color: Theme.of(context).buttonColor,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 8,
                                          ),
                                          child: Text(
                                            "Festive Deal 1",
                                            style: TextStyle(
                                              color: Theme.of(context).buttonColor,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(0),
                                          child: Text(
                                            "Rs. 548.00",
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(top: 15),
                                          child: SizedBox(
                                            height: 30,
                                            child: ElevatedButton(
                                              style: ButtonStyle(
                                                backgroundColor:
                                                    MaterialStateProperty
                                                        .resolveWith<Color>(
                                                  (Set<MaterialState> states) {
                                                    return states.contains(
                                                            MaterialState
                                                                .pressed)
                                                        ? Theme.of(context).accentColor
                                                        : Theme.of(context).accentColor;
                                                  },
                                                ),
                                                shape:
                                                    MaterialStateProperty.all<
                                                        RoundedRectangleBorder>(
                                                  RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    side: BorderSide(
                                                      color:
                                                          Theme.of(context).accentColor,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              onPressed: () {},
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    "Reorder",
                                                    style: TextStyle(
                                                      color: Theme.of(context).primaryColorDark,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontFamily: 'Poppins',
                                                      letterSpacing: 0.5,
                                                      fontSize: 13,
                                                      height: 1,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                color: Theme.of(context).cardColor,
                                child: Padding(
                                  padding: EdgeInsets.only(
                                    top: 15,
                                    bottom: 15,
                                  ),
                                  child: Divider(
                                    height: 1,
                                    color: Theme.of(context).primaryColorDark,
                                    thickness: 1,
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => FoodDetailScreen(),
                                    ),
                                  );
                                },
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(0),
                                          child: Text(
                                            "Broadway Pizza - Malir Cantt",
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 8,
                                          ),
                                          child: Text(
                                            "15 May, 15:58",
                                            style: TextStyle(
                                              color: Theme.of(context).buttonColor,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 8,
                                          ),
                                          child: Text(
                                            "Festive Deal 1",
                                            style: TextStyle(
                                              color: Theme.of(context).buttonColor,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(0),
                                          child: Text(
                                            "Rs. 548.00",
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(top: 15),
                                          child: SizedBox(
                                            height: 30,
                                            child: ElevatedButton(
                                              style: ButtonStyle(
                                                backgroundColor:
                                                    MaterialStateProperty
                                                        .resolveWith<Color>(
                                                  (Set<MaterialState> states) {
                                                    return states.contains(
                                                            MaterialState
                                                                .pressed)
                                                        ? Theme.of(context).accentColor
                                                        : Theme.of(context).accentColor;
                                                  },
                                                ),
                                                shape:
                                                    MaterialStateProperty.all<
                                                        RoundedRectangleBorder>(
                                                  RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    side: BorderSide(
                                                      color:
                                                          Theme.of(context).accentColor,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              onPressed: () {},
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    "Reorder",
                                                    style: TextStyle(
                                                      color: Theme.of(context).primaryColorDark,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontFamily: 'Poppins',
                                                      letterSpacing: 0.5,
                                                      fontSize: 13,
                                                      height: 1,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                color: Theme.of(context).cardColor,
                                child: Padding(
                                  padding: EdgeInsets.only(
                                    top: 15,
                                    bottom: 15,
                                  ),
                                  child: Divider(
                                    height: 1,
                                    color: Theme.of(context).primaryColorDark,
                                    thickness: 1,
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => FoodDetailScreen(),
                                    ),
                                  );
                                },
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(0),
                                          child: Text(
                                            "Broadway Pizza - Malir Cantt",
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 8,
                                          ),
                                          child: Text(
                                            "15 May, 15:58",
                                            style: TextStyle(
                                              color: Theme.of(context).buttonColor,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 8,
                                          ),
                                          child: Text(
                                            "Festive Deal 1",
                                            style: TextStyle(
                                              color: Theme.of(context).buttonColor,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(0),
                                          child: Text(
                                            "Rs. 548.00",
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(top: 15),
                                          child: SizedBox(
                                            height: 30,
                                            child: ElevatedButton(
                                              style: ButtonStyle(
                                                backgroundColor:
                                                    MaterialStateProperty
                                                        .resolveWith<Color>(
                                                  (Set<MaterialState> states) {
                                                    return states.contains(
                                                            MaterialState
                                                                .pressed)
                                                        ? Theme.of(context).accentColor
                                                        : Theme.of(context).accentColor;
                                                  },
                                                ),
                                                shape:
                                                    MaterialStateProperty.all<
                                                        RoundedRectangleBorder>(
                                                  RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    side: BorderSide(
                                                      color:
                                                          Theme.of(context).accentColor,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              onPressed: () {},
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    "Reorder",
                                                    style: TextStyle(
                                                      color: Theme.of(context).primaryColorDark,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontFamily: 'Poppins',
                                                      letterSpacing: 0.5,
                                                      fontSize: 13,
                                                      height: 1,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).primaryColor,
      elevation: 0,
      centerTitle: true,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          color: Theme.of(context).primaryColorDark,
          size: 18,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      title: Text(
        "Orders",
        style: TextStyle(
          color: Theme.of(context).primaryColorDark,
          fontWeight: FontWeight.w600,
          fontSize: 16,
          fontFamily: 'Poppins',
          letterSpacing: 0.3,
          height: 1,
        ),
      ),
    );
  }
}
