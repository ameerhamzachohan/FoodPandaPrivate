import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FAQScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Scaffold(
      body: SafeArea(
        child: DefaultTabController(
          length: 2,
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  leading: IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      size: 22,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  expandedHeight: mediaQuery.size.height * 0.3,
                  floating: false,
                  pinned: true,
                  backgroundColor: Theme.of(context).primaryColor,
                  flexibleSpace: FlexibleSpaceBar(
                    background: Container(
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          // colorFilter: new ColorFilter.mode(
                          //   Theme.of(context).primaryColor12.withOpacity(1),
                          //   BlendMode.dstATop,
                          // ),
                          image: AssetImage(
                            'assets/images/how.png',
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ];
            },
            body: Container(
              color: Theme.of(context).primaryColorDark,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "What’s the difference between: Basic/ \nfor Standard/ Pro/ Premium / Legendary",
                          maxLines: 2,
                          style: TextStyle(
                              color: Theme.of(context).buttonColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              fontFamily: 'Poppins',
                              letterSpacing: 0.3,
                              height: 1.3),
                        ),
                        Image.asset(
                          "assets/images/icon-info-forward.png",
                          width: 10,
                          height: 10,
                          color: Theme.of(context).buttonColor,
                        ),
                      ],
                    ),
                    Container(
                      color: Theme.of(context).primaryColorDark,
                      child: Padding(
                        padding: EdgeInsets.only(
                          top: 20,
                          bottom: 20,
                        ),
                        child: Divider(
                          height: 1,
                          thickness: 1,
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "When will my Supply drop Ship?",
                          maxLines: 2,
                          style: TextStyle(
                              color: Theme.of(context).buttonColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              fontFamily: 'Poppins',
                              letterSpacing: 0.3,
                              height: 1.3),
                        ),
                        Image.asset(
                          "assets/images/icon-info-forward.png",
                          width: 10,
                          height: 10,
                          color: Theme.of(context).buttonColor,
                        ),
                      ],
                    ),
                    Container(
                      color: Theme.of(context).primaryColorDark,
                      child: Padding(
                        padding: EdgeInsets.only(
                          top: 20,
                          bottom: 20,
                        ),
                        child: Divider(
                          height: 1,
                          thickness: 1,
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "How will I know when my supply drop has \nfor shipped? How do I Track my Shipment?",
                          maxLines: 2,
                          style: TextStyle(
                              color: Theme.of(context).buttonColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              fontFamily: 'Poppins',
                              letterSpacing: 0.3,
                              height: 1.3),
                        ),
                        Image.asset(
                          "assets/images/icon-info-forward.png",
                          width: 10,
                          height: 10,
                          color: Theme.of(context).buttonColor,
                        ),
                      ],
                    ),
                    Container(
                      color: Theme.of(context).primaryColorDark,
                      child: Padding(
                        padding: EdgeInsets.only(
                          top: 20,
                          bottom: 20,
                        ),
                        child: Divider(
                          height: 1,
                          thickness: 1,
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "How can I change my Shipping Address or \nfor Pause / Cancel My Shipments?",
                          maxLines: 2,
                          style: TextStyle(
                              color: Theme.of(context).buttonColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              fontFamily: 'Poppins',
                              letterSpacing: 0.3,
                              height: 1.3),
                        ),
                        Image.asset(
                          "assets/images/icon-info-forward.png",
                          width: 10,
                          height: 10,
                          color: Theme.of(context).buttonColor,
                        ),
                      ],
                    ),
                    Container(
                      color: Theme.of(context).primaryColorDark,
                      child: Padding(
                        padding: EdgeInsets.only(
                          top: 20,
                          bottom: 20,
                        ),
                        child: Divider(
                          height: 1,
                          thickness: 1,
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "What if I have Questions about My Order?",
                          maxLines: 2,
                          style: TextStyle(
                              color: Theme.of(context).buttonColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              fontFamily: 'Poppins',
                              letterSpacing: 0.3,
                              height: 1.3),
                        ),
                        Image.asset(
                          "assets/images/icon-info-forward.png",
                          width: 10,
                          height: 10,
                          color: Theme.of(context).buttonColor,
                        ),
                      ],
                    ),
                    Container(
                      color: Theme.of(context).primaryColorDark,
                      child: Padding(
                        padding: EdgeInsets.only(
                          top: 20,
                          bottom: 20,
                        ),
                        child: Divider(
                          height: 1,
                          thickness: 1,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
