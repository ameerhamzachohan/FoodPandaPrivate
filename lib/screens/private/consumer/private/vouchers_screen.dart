import 'package:flutter/material.dart';

class VouchersScreen extends StatefulWidget {
  @override
  _VouchersScreenState createState() => _VouchersScreenState();
}

class _VouchersScreenState extends State<VouchersScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: SafeArea(
        top: true,
        bottom: true,
        child: DefaultTabController(
          length: 2,
          child: Column(
            children: [
              Container(
                color: Theme.of(context).cardColor,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TabBar(
                      labelColor: Theme.of(context).accentColor,
                      indicatorColor: Theme.of(context).accentColor,
                      unselectedLabelColor: Theme.of(context).primaryColorDark,
                      tabs: [
                        Tab(
                          child: Text(
                            "Current",
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w600,
                              fontFamily: "Poppins",
                              height: 1,
                            ),
                          ),
                        ),
                        Tab(
                          child: Text(
                            "Past",
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w600,
                              fontFamily: "Poppins",
                              height: 1,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  color: Theme.of(context).primaryColor,
                  child: TabBarView(
                    children: [
                      SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 20,
                            right: 20,
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    top: 20,
                                    left: 10,
                                    right: 10,
                                    bottom: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).cardColor,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5),
                                      bottomLeft: Radius.circular(5),
                                      bottomRight: Radius.circular(5),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Theme.of(context).buttonColor.withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 5,
                                        offset: Offset(
                                          0,
                                          0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Welcome Back",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                          Text(
                                            "Rs. 694.00",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "V11GA",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 8,
                                                right: 8,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context).accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(2),
                                                  topLeft: Radius.circular(2),
                                                  bottomRight:
                                                      Radius.circular(2),
                                                  bottomLeft:
                                                      Radius.circular(2),
                                                ),
                                              ),
                                              child: Text(
                                                "EXPIRED",
                                                style: TextStyle(
                                                  height: 1,
                                                  fontSize: 12,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  fontWeight: FontWeight.w500,
                                                  color: Theme.of(context).primaryColorDark,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Rs. 694.00",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Text(
                                              "Valid Until 29-Oct-2020",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        color: Theme.of(context).cardColor,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            top: 15,
                                            bottom: 15,
                                          ),
                                          child: Divider(
                                            height: 1,
                                            color: Theme.of(context).primaryColorDark,
                                            thickness: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 0,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "PandaMart",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).accentColor,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    top: 20,
                                    left: 10,
                                    right: 10,
                                    bottom: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).cardColor,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5),
                                      bottomLeft: Radius.circular(5),
                                      bottomRight: Radius.circular(5),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Theme.of(context).buttonColor.withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 5,
                                        offset: Offset(
                                          0,
                                          0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Welcome Back",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                          Text(
                                            "Rs. 694.00",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "V11GA",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 8,
                                                right: 8,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context).accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(2),
                                                  topLeft: Radius.circular(2),
                                                  bottomRight:
                                                      Radius.circular(2),
                                                  bottomLeft:
                                                      Radius.circular(2),
                                                ),
                                              ),
                                              child: Text(
                                                "EXPIRED",
                                                style: TextStyle(
                                                  height: 1,
                                                  fontSize: 12,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  fontWeight: FontWeight.w500,
                                                  color: Theme.of(context).primaryColorDark,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Rs. 694.00",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Text(
                                              "Valid Until 29-Oct-2020",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        color: Theme.of(context).cardColor,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            top: 15,
                                            bottom: 15,
                                          ),
                                          child: Divider(
                                            height: 1,
                                            color: Theme.of(context).primaryColorDark,
                                            thickness: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 0,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "PandaMart",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).accentColor,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    top: 20,
                                    left: 10,
                                    right: 10,
                                    bottom: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).cardColor,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5),
                                      bottomLeft: Radius.circular(5),
                                      bottomRight: Radius.circular(5),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Theme.of(context).buttonColor.withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 5,
                                        offset: Offset(
                                          0,
                                          0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Welcome Back",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                          Text(
                                            "Rs. 694.00",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "V11GA",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 8,
                                                right: 8,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context).accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(2),
                                                  topLeft: Radius.circular(2),
                                                  bottomRight:
                                                      Radius.circular(2),
                                                  bottomLeft:
                                                      Radius.circular(2),
                                                ),
                                              ),
                                              child: Text(
                                                "EXPIRED",
                                                style: TextStyle(
                                                  height: 1,
                                                  fontSize: 12,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  fontWeight: FontWeight.w500,
                                                  color: Theme.of(context).primaryColorDark,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Rs. 694.00",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Text(
                                              "Valid Until 29-Oct-2020",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        color: Theme.of(context).cardColor,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            top: 15,
                                            bottom: 15,
                                          ),
                                          child: Divider(
                                            height: 1,
                                            color: Theme.of(context).primaryColorDark,
                                            thickness: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 0,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "PandaMart",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).accentColor,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    top: 20,
                                    left: 10,
                                    right: 10,
                                    bottom: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).cardColor,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5),
                                      bottomLeft: Radius.circular(5),
                                      bottomRight: Radius.circular(5),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Theme.of(context).buttonColor.withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 5,
                                        offset: Offset(
                                          0,
                                          0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Welcome Back",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                          Text(
                                            "Rs. 694.00",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "V11GA",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 8,
                                                right: 8,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context).accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(2),
                                                  topLeft: Radius.circular(2),
                                                  bottomRight:
                                                      Radius.circular(2),
                                                  bottomLeft:
                                                      Radius.circular(2),
                                                ),
                                              ),
                                              child: Text(
                                                "EXPIRED",
                                                style: TextStyle(
                                                  height: 1,
                                                  fontSize: 12,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  fontWeight: FontWeight.w500,
                                                  color: Theme.of(context).primaryColorDark,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Rs. 694.00",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Text(
                                              "Valid Until 29-Oct-2020",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        color: Theme.of(context).cardColor,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            top: 15,
                                            bottom: 15,
                                          ),
                                          child: Divider(
                                            height: 1,
                                            color: Theme.of(context).primaryColorDark,
                                            thickness: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 0,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "PandaMart",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).accentColor,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    top: 20,
                                    left: 10,
                                    right: 10,
                                    bottom: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).cardColor,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5),
                                      bottomLeft: Radius.circular(5),
                                      bottomRight: Radius.circular(5),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Theme.of(context).buttonColor.withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 5,
                                        offset: Offset(
                                          0,
                                          0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Welcome Back",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                          Text(
                                            "Rs. 694.00",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "V11GA",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 8,
                                                right: 8,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context).accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(2),
                                                  topLeft: Radius.circular(2),
                                                  bottomRight:
                                                      Radius.circular(2),
                                                  bottomLeft:
                                                      Radius.circular(2),
                                                ),
                                              ),
                                              child: Text(
                                                "EXPIRED",
                                                style: TextStyle(
                                                  height: 1,
                                                  fontSize: 12,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  fontWeight: FontWeight.w500,
                                                  color: Theme.of(context).primaryColorDark,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Rs. 694.00",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Text(
                                              "Valid Until 29-Oct-2020",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        color: Theme.of(context).cardColor,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            top: 15,
                                            bottom: 15,
                                          ),
                                          child: Divider(
                                            height: 1,
                                            color: Theme.of(context).primaryColorDark,
                                            thickness: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 0,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "PandaMart",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).accentColor,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    top: 20,
                                    left: 10,
                                    right: 10,
                                    bottom: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).cardColor,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5),
                                      bottomLeft: Radius.circular(5),
                                      bottomRight: Radius.circular(5),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Theme.of(context).buttonColor.withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 5,
                                        offset: Offset(
                                          0,
                                          0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Welcome Back",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                          Text(
                                            "Rs. 694.00",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "V11GA",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 8,
                                                right: 8,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context).accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(2),
                                                  topLeft: Radius.circular(2),
                                                  bottomRight:
                                                      Radius.circular(2),
                                                  bottomLeft:
                                                      Radius.circular(2),
                                                ),
                                              ),
                                              child: Text(
                                                "EXPIRED",
                                                style: TextStyle(
                                                  height: 1,
                                                  fontSize: 12,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  fontWeight: FontWeight.w500,
                                                  color: Theme.of(context).primaryColorDark,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Rs. 694.00",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Text(
                                              "Valid Until 29-Oct-2020",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        color: Theme.of(context).cardColor,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            top: 15,
                                            bottom: 15,
                                          ),
                                          child: Divider(
                                            height: 1,
                                            color: Theme.of(context).primaryColorDark,
                                            thickness: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 0,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "PandaMart",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).accentColor,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 20,
                            right: 20,
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    top: 20,
                                    left: 10,
                                    right: 10,
                                    bottom: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).cardColor,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5),
                                      bottomLeft: Radius.circular(5),
                                      bottomRight: Radius.circular(5),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Theme.of(context).buttonColor.withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 5,
                                        offset: Offset(
                                          0,
                                          0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Welcome Back",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                          Text(
                                            "Rs. 694.00",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "V11GA",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 8,
                                                right: 8,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context).accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(2),
                                                  topLeft: Radius.circular(2),
                                                  bottomRight:
                                                      Radius.circular(2),
                                                  bottomLeft:
                                                      Radius.circular(2),
                                                ),
                                              ),
                                              child: Text(
                                                "EXPIRED",
                                                style: TextStyle(
                                                  height: 1,
                                                  fontSize: 12,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  fontWeight: FontWeight.w500,
                                                  color: Theme.of(context).primaryColorDark,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Rs. 694.00",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Text(
                                              "Valid Until 29-Oct-2020",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        color: Theme.of(context).cardColor,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            top: 15,
                                            bottom: 15,
                                          ),
                                          child: Divider(
                                            height: 1,
                                            color: Theme.of(context).primaryColorDark,
                                            thickness: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 0,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "PandaMart",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).accentColor,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    top: 20,
                                    left: 10,
                                    right: 10,
                                    bottom: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).cardColor,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5),
                                      bottomLeft: Radius.circular(5),
                                      bottomRight: Radius.circular(5),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Theme.of(context).buttonColor.withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 5,
                                        offset: Offset(
                                          0,
                                          0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Welcome Back",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                          Text(
                                            "Rs. 694.00",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "V11GA",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 8,
                                                right: 8,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context).accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(2),
                                                  topLeft: Radius.circular(2),
                                                  bottomRight:
                                                      Radius.circular(2),
                                                  bottomLeft:
                                                      Radius.circular(2),
                                                ),
                                              ),
                                              child: Text(
                                                "EXPIRED",
                                                style: TextStyle(
                                                  height: 1,
                                                  fontSize: 12,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  fontWeight: FontWeight.w500,
                                                  color: Theme.of(context).primaryColorDark,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Rs. 694.00",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Text(
                                              "Valid Until 29-Oct-2020",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        color: Theme.of(context).cardColor,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            top: 15,
                                            bottom: 15,
                                          ),
                                          child: Divider(
                                            height: 1,
                                            color: Theme.of(context).primaryColorDark,
                                            thickness: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 0,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "PandaMart",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).accentColor,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    top: 20,
                                    left: 10,
                                    right: 10,
                                    bottom: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).cardColor,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5),
                                      bottomLeft: Radius.circular(5),
                                      bottomRight: Radius.circular(5),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Theme.of(context).buttonColor.withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 5,
                                        offset: Offset(
                                          0,
                                          0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Welcome Back",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                          Text(
                                            "Rs. 694.00",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "V11GA",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 8,
                                                right: 8,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context).accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(2),
                                                  topLeft: Radius.circular(2),
                                                  bottomRight:
                                                      Radius.circular(2),
                                                  bottomLeft:
                                                      Radius.circular(2),
                                                ),
                                              ),
                                              child: Text(
                                                "EXPIRED",
                                                style: TextStyle(
                                                  height: 1,
                                                  fontSize: 12,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  fontWeight: FontWeight.w500,
                                                  color: Theme.of(context).primaryColorDark,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Rs. 694.00",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Text(
                                              "Valid Until 29-Oct-2020",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        color: Theme.of(context).cardColor,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            top: 15,
                                            bottom: 15,
                                          ),
                                          child: Divider(
                                            height: 1,
                                            color: Theme.of(context).primaryColorDark,
                                            thickness: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 0,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "PandaMart",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).accentColor,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    top: 20,
                                    left: 10,
                                    right: 10,
                                    bottom: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).cardColor,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5),
                                      bottomLeft: Radius.circular(5),
                                      bottomRight: Radius.circular(5),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Theme.of(context).buttonColor.withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 5,
                                        offset: Offset(
                                          0,
                                          0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Welcome Back",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                          Text(
                                            "Rs. 694.00",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "V11GA",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 8,
                                                right: 8,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context).accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(2),
                                                  topLeft: Radius.circular(2),
                                                  bottomRight:
                                                      Radius.circular(2),
                                                  bottomLeft:
                                                      Radius.circular(2),
                                                ),
                                              ),
                                              child: Text(
                                                "EXPIRED",
                                                style: TextStyle(
                                                  height: 1,
                                                  fontSize: 12,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  fontWeight: FontWeight.w500,
                                                  color: Theme.of(context).primaryColorDark,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Rs. 694.00",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Text(
                                              "Valid Until 29-Oct-2020",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        color: Theme.of(context).cardColor,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            top: 15,
                                            bottom: 15,
                                          ),
                                          child: Divider(
                                            height: 1,
                                            color: Theme.of(context).primaryColorDark,
                                            thickness: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 0,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "PandaMart",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).accentColor,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    top: 20,
                                    left: 10,
                                    right: 10,
                                    bottom: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).cardColor,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5),
                                      bottomLeft: Radius.circular(5),
                                      bottomRight: Radius.circular(5),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Theme.of(context).buttonColor.withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 5,
                                        offset: Offset(
                                          0,
                                          0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Welcome Back",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                          Text(
                                            "Rs. 694.00",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "V11GA",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 8,
                                                right: 8,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context).accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(2),
                                                  topLeft: Radius.circular(2),
                                                  bottomRight:
                                                      Radius.circular(2),
                                                  bottomLeft:
                                                      Radius.circular(2),
                                                ),
                                              ),
                                              child: Text(
                                                "EXPIRED",
                                                style: TextStyle(
                                                  height: 1,
                                                  fontSize: 12,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  fontWeight: FontWeight.w500,
                                                  color: Theme.of(context).primaryColorDark,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Rs. 694.00",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Text(
                                              "Valid Until 29-Oct-2020",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        color: Theme.of(context).cardColor,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            top: 15,
                                            bottom: 15,
                                          ),
                                          child: Divider(
                                            height: 1,
                                            color: Theme.of(context).primaryColorDark,
                                            thickness: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 0,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "PandaMart",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).accentColor,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    top: 20,
                                    left: 10,
                                    right: 10,
                                    bottom: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).cardColor,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5),
                                      bottomLeft: Radius.circular(5),
                                      bottomRight: Radius.circular(5),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Theme.of(context).buttonColor.withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 5,
                                        offset: Offset(
                                          0,
                                          0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Welcome Back",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                          Text(
                                            "Rs. 694.00",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Theme.of(context).primaryColorDark,
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              letterSpacing: 0,
                                              height: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "V11GA",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 8,
                                                right: 8,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context).accentColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(2),
                                                  topLeft: Radius.circular(2),
                                                  bottomRight:
                                                      Radius.circular(2),
                                                  bottomLeft:
                                                      Radius.circular(2),
                                                ),
                                              ),
                                              child: Text(
                                                "EXPIRED",
                                                style: TextStyle(
                                                  height: 1,
                                                  fontSize: 12,
                                                  fontFamily: 'Poppins',
                                                  letterSpacing: 0.3,
                                                  fontWeight: FontWeight.w500,
                                                  color: Theme.of(context).primaryColorDark,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 12,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Rs. 694.00",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                            Text(
                                              "Valid Until 29-Oct-2020",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColorDark,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        color: Theme.of(context).cardColor,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            top: 15,
                                            bottom: 15,
                                          ),
                                          child: Divider(
                                            height: 1,
                                            color: Theme.of(context).primaryColorDark,
                                            thickness: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 0,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "PandaMart",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Theme.of(context).accentColor,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16,
                                                fontFamily: 'Poppins',
                                                letterSpacing: 0,
                                                height: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).primaryColor,
      elevation: 0,
      centerTitle: true,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          color: Theme.of(context).primaryColorDark,
          size: 18,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      title: Text(
        "VOUCHERS",
        style: TextStyle(
          color: Theme.of(context).primaryColorDark,
          fontWeight: FontWeight.w600,
          fontSize: 16,
          fontFamily: 'Poppins',
          letterSpacing: 0.3,
          height: 1,
        ),
      ),
    );
  }
}
