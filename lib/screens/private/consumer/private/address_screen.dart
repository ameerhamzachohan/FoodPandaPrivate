import 'package:flutter/material.dart';

class AddressScreen extends StatefulWidget {
  @override
  _AddressScreenState createState() => _AddressScreenState();
}

class _AddressScreenState extends State<AddressScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: Container(
        color: Theme.of(context).primaryColor,
        width: double.infinity,
        height: double.infinity,
        child: SafeArea(
          top: true,
          bottom: true,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 0,
                              bottom: 0,
                            ),
                            child: Divider(
                              height: 1,
                              thickness: 1,
                              color: Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                        Container(
                          color: Theme.of(context).cardColor,
                          padding: EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 15,
                            right: 15,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.home,
                                      color: Theme.of(context).accentColor,
                                      size: 25,
                                    )
                                  ],
                                ),
                              ),
                              Flexible(
                                flex: 12,
                                fit: FlexFit.tight,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 5,
                                        ),
                                        child: Text(
                                          "Broadway Pizza - Malir Cantt",
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "China Town Building Mod Apartment..",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "Note to rider: Call me at 03242492566",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.edit,
                                      size: 25,
                                      color: Theme.of(context).accentColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 0,
                              bottom: 0,
                            ),
                            child: Divider(
                              height: 1,
                              thickness: 1,
                              color: Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                        Container(
                          color: Theme.of(context).cardColor,
                          padding: EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 15,
                            right: 15,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.home,
                                      color: Theme.of(context).accentColor,
                                      size: 25,
                                    )
                                  ],
                                ),
                              ),
                              Flexible(
                                flex: 12,
                                fit: FlexFit.tight,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 5,
                                        ),
                                        child: Text(
                                          "Broadway Pizza - Malir Cantt",
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "China Town Building Mod Apartment..",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "Note to rider: Call me at 03242492566",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.edit,
                                      size: 25,
                                      color: Theme.of(context).accentColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 0,
                              bottom: 0,
                            ),
                            child: Divider(
                              height: 1,
                              thickness: 1,
                              color: Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                        Container(
                          color: Theme.of(context).cardColor,
                          padding: EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 15,
                            right: 15,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.home,
                                      color: Theme.of(context).accentColor,
                                      size: 25,
                                    )
                                  ],
                                ),
                              ),
                              Flexible(
                                flex: 12,
                                fit: FlexFit.tight,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 5,
                                        ),
                                        child: Text(
                                          "Broadway Pizza - Malir Cantt",
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "China Town Building Mod Apartment..",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "Note to rider: Call me at 03242492566",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.edit,
                                      size: 25,
                                      color: Theme.of(context).accentColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 0,
                              bottom: 0,
                            ),
                            child: Divider(
                              height: 1,
                              thickness: 1,
                              color: Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                        Container(
                          color: Theme.of(context).cardColor,
                          padding: EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 15,
                            right: 15,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.home,
                                      color: Theme.of(context).accentColor,
                                      size: 25,
                                    )
                                  ],
                                ),
                              ),
                              Flexible(
                                flex: 12,
                                fit: FlexFit.tight,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 5,
                                        ),
                                        child: Text(
                                          "Broadway Pizza - Malir Cantt",
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "China Town Building Mod Apartment..",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "Note to rider: Call me at 03242492566",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.edit,
                                      size: 25,
                                      color: Theme.of(context).accentColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 0,
                              bottom: 0,
                            ),
                            child: Divider(
                              height: 1,
                              thickness: 1,
                              color: Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                        Container(
                          color: Theme.of(context).cardColor,
                          padding: EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 15,
                            right: 15,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.home,
                                      color: Theme.of(context).accentColor,
                                      size: 25,
                                    )
                                  ],
                                ),
                              ),
                              Flexible(
                                flex: 12,
                                fit: FlexFit.tight,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 5,
                                        ),
                                        child: Text(
                                          "Broadway Pizza - Malir Cantt",
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "China Town Building Mod Apartment..",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "Note to rider: Call me at 03242492566",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.edit,
                                      size: 25,
                                      color: Theme.of(context).accentColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 0,
                              bottom: 0,
                            ),
                            child: Divider(
                              height: 1,
                              thickness: 1,
                              color: Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                        Container(
                          color: Theme.of(context).cardColor,
                          padding: EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 15,
                            right: 15,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.home,
                                      color: Theme.of(context).accentColor,
                                      size: 25,
                                    )
                                  ],
                                ),
                              ),
                              Flexible(
                                flex: 12,
                                fit: FlexFit.tight,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 5,
                                        ),
                                        child: Text(
                                          "Broadway Pizza - Malir Cantt",
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "China Town Building Mod Apartment..",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "Note to rider: Call me at 03242492566",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.edit,
                                      size: 25,
                                      color: Theme.of(context).accentColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 0,
                              bottom: 0,
                            ),
                            child: Divider(
                              height: 1,
                              thickness: 1,
                              color: Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                        Container(
                          color: Theme.of(context).cardColor,
                          padding: EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 15,
                            right: 15,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.home,
                                      color: Theme.of(context).accentColor,
                                      size: 25,
                                    )
                                  ],
                                ),
                              ),
                              Flexible(
                                flex: 12,
                                fit: FlexFit.tight,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 5,
                                        ),
                                        child: Text(
                                          "Broadway Pizza - Malir Cantt",
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "China Town Building Mod Apartment..",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "Note to rider: Call me at 03242492566",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.edit,
                                      size: 25,
                                      color: Theme.of(context).accentColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 0,
                              bottom: 0,
                            ),
                            child: Divider(
                              height: 1,
                              thickness: 1,
                              color: Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                        Container(
                          color: Theme.of(context).cardColor,
                          padding: EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 15,
                            right: 15,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.home,
                                      color: Theme.of(context).accentColor,
                                      size: 25,
                                    )
                                  ],
                                ),
                              ),
                              Flexible(
                                flex: 12,
                                fit: FlexFit.tight,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 5,
                                        ),
                                        child: Text(
                                          "Broadway Pizza - Malir Cantt",
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "China Town Building Mod Apartment..",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "Note to rider: Call me at 03242492566",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.edit,
                                      size: 25,
                                      color: Theme.of(context).accentColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 0,
                              bottom: 0,
                            ),
                            child: Divider(
                              height: 1,
                              thickness: 1,
                              color: Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                        Container(
                          color: Theme.of(context).cardColor,
                          padding: EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 15,
                            right: 15,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.home,
                                      color: Theme.of(context).accentColor,
                                      size: 25,
                                    )
                                  ],
                                ),
                              ),
                              Flexible(
                                flex: 12,
                                fit: FlexFit.tight,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 5,
                                        ),
                                        child: Text(
                                          "Broadway Pizza - Malir Cantt",
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "China Town Building Mod Apartment..",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "Note to rider: Call me at 03242492566",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.edit,
                                      size: 25,
                                      color: Theme.of(context).accentColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 0,
                              bottom: 0,
                            ),
                            child: Divider(
                              height: 1,
                              thickness: 1,
                              color: Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                        Container(
                          color: Theme.of(context).cardColor,
                          padding: EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 15,
                            right: 15,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.home,
                                      color: Theme.of(context).accentColor,
                                      size: 25,
                                    )
                                  ],
                                ),
                              ),
                              Flexible(
                                flex: 12,
                                fit: FlexFit.tight,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 5,
                                        ),
                                        child: Text(
                                          "Broadway Pizza - Malir Cantt",
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "China Town Building Mod Apartment..",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "Note to rider: Call me at 03242492566",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.edit,
                                      size: 25,
                                      color: Theme.of(context).accentColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 0,
                              bottom: 0,
                            ),
                            child: Divider(
                              height: 1,
                              thickness: 1,
                              color: Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                        Container(
                          color: Theme.of(context).cardColor,
                          padding: EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 15,
                            right: 15,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.home,
                                      color: Theme.of(context).accentColor,
                                      size: 25,
                                    )
                                  ],
                                ),
                              ),
                              Flexible(
                                flex: 12,
                                fit: FlexFit.tight,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 5,
                                        ),
                                        child: Text(
                                          "Broadway Pizza - Malir Cantt",
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "China Town Building Mod Apartment..",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "Note to rider: Call me at 03242492566",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.edit,
                                      size: 25,
                                      color: Theme.of(context).accentColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 0,
                              bottom: 0,
                            ),
                            child: Divider(
                              height: 1,
                              thickness: 1,
                              color: Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                        Container(
                          color: Theme.of(context).cardColor,
                          padding: EdgeInsets.only(
                            top: 15,
                            bottom: 15,
                            left: 15,
                            right: 15,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.home,
                                      color: Theme.of(context).accentColor,
                                      size: 25,
                                    )
                                  ],
                                ),
                              ),
                              Flexible(
                                flex: 12,
                                fit: FlexFit.tight,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 10,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 5,
                                        ),
                                        child: Text(
                                          "Broadway Pizza - Malir Cantt",
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "China Town Building Mod Apartment..",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 8,
                                        ),
                                        child: Text(
                                          "Note to rider: Call me at 03242492566",
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).buttonColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                fit: FlexFit.loose,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.edit,
                                      size: 25,
                                      color: Theme.of(context).accentColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 0,
                              bottom: 0,
                            ),
                            child: Divider(
                              height: 1,
                              thickness: 1,
                              color: Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  color: Theme.of(context).cardColor,
                  padding: EdgeInsets.only(
                    top: 15,
                    left: 20,
                    right: 20,
                    bottom: 15,
                  ),
                  child: SizedBox(
                    width: double.infinity,
                    height: 45,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) {
                            return states.contains(MaterialState.pressed)
                                ? Theme.of(context).accentColor
                                : Theme.of(context).accentColor;
                          },
                        ),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0),
                            side: BorderSide(
                              color: Theme.of(context).accentColor,
                            ),
                          ),
                        ),
                      ),
                      onPressed: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "ADD NEW ADDRESS",
                            style: TextStyle(
                              color: Theme.of(context).primaryColorDark,
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Poppins',
                              letterSpacing: 0.5,
                              fontSize: 13,
                              height: 1,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).primaryColor,
      elevation: 0,
      centerTitle: true,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          color: Theme.of(context).primaryColorDark,
          size: 18,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      title: Text(
        "Address",
        style: TextStyle(
          color: Theme.of(context).primaryColorDark,
          fontWeight: FontWeight.w600,
          fontSize: 16,
          fontFamily: 'Poppins',
          letterSpacing: 0.3,
          height: 1,
        ),
      ),
    );
  }
}
