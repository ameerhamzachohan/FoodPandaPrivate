import 'package:flutter/material.dart';
import 'package:foodpanda/screens/private/consumer/private/privacy_policy_screen.dart';
import 'package:foodpanda/screens/private/consumer/private/terms_condition_screen.dart';

class MoreScreen extends StatefulWidget {
  @override
  _MoreScreenState createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: SafeArea(
        top: true,
        bottom: true,
        child: Container(
          color: Theme.of(context).primaryColor,
          width: double.infinity,
          height: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        color: Theme.of(context).primaryColorDark,
                        child: Padding(
                          padding: EdgeInsets.only(
                            top: 5,
                            bottom: 0,
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => TermsAndConditionScreen(),
                            ),
                          );
                        },
                        child: Padding(
                          padding: EdgeInsets.only(
                            top: 0,
                            left: 0,
                            right: 0,
                          ),
                          child: Container(
                            color: Theme.of(context).cardColor,
                            child: Padding(
                              padding: const EdgeInsets.only(
                                top: 20,
                                bottom: 20,
                                left: 20,
                                right: 20,
                              ),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Terms & Conditions",
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColorDark,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      fontFamily: 'Poppins',
                                      letterSpacing: 0.3,
                                      height: 1,
                                    ),
                                  ),
                                  Image.asset(
                                    "assets/images/right-arrow.png",
                                    width: 15,
                                    height: 15,
                                    color: Theme.of(context).accentColor,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        color: Theme.of(context).primaryColorDark,
                        child: Padding(
                          padding: EdgeInsets.only(
                            top: 5,
                            bottom: 0,
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PrivacyPolicyScreen(),
                            ),
                          );
                        },
                        child: Padding(
                          padding: EdgeInsets.only(
                            top: 0,
                            left: 0,
                            right: 0,
                          ),
                          child: Container(
                            color: Theme.of(context).cardColor,
                            child: Padding(
                              padding: const EdgeInsets.only(
                                top: 20,
                                bottom: 20,
                                left: 20,
                                right: 20,
                              ),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Data Policy",
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColorDark,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      fontFamily: 'Poppins',
                                      letterSpacing: 0.3,
                                      height: 1,
                                    ),
                                  ),
                                  Image.asset(
                                    "assets/images/right-arrow.png",
                                    width: 15,
                                    height: 15,
                                    color: Theme.of(context).accentColor,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        color: Theme.of(context).primaryColorDark,
                        child: Padding(
                          padding: EdgeInsets.only(
                            top: 5,
                            bottom: 0,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          top: 0,
                          left: 0,
                          right: 0,
                        ),
                        child: Container(
                          color: Theme.of(context).cardColor,
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 20,
                              bottom: 20,
                              left: 20,
                              right: 20,
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "FAQ",
                                  style: TextStyle(
                                    color: Theme.of(context).primaryColorDark,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14,
                                    fontFamily: 'Poppins',
                                    letterSpacing: 0.3,
                                    height: 1,
                                  ),
                                ),
                                Image.asset(
                                  "assets/images/right-arrow.png",
                                  width: 15,
                                  height: 15,
                                  color: Theme.of(context).accentColor,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        color: Theme.of(context).primaryColorDark,
                        child: Padding(
                          padding: EdgeInsets.only(
                            top: 5,
                            bottom: 0,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).primaryColor,
      elevation: 0,
      centerTitle: true,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          color: Theme.of(context).primaryColorDark,
          size: 18,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      title: Text(
        "More",
        style: TextStyle(
          color: Theme.of(context).primaryColorDark,
          fontWeight: FontWeight.w600,
          fontSize: 16,
          fontFamily: 'Poppins',
          letterSpacing: 0.3,
          height: 1,
        ),
      ),
    );
  }
}
