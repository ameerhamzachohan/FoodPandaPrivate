import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodpanda/screens/private/consumer/private/detail_screen.dart';

class CutomizeScreen extends StatefulWidget {
  @override
  _CutomizeScreenState createState() => _CutomizeScreenState();
}

class _CutomizeScreenState extends State<CutomizeScreen> {
  @override
  Widget build(BuildContext context) {
    bool _value2 = true;

    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
        return Future.value(false);
      },
      child: Scaffold(
        body: DefaultTabController(
          length: 2,
          child: NestedScrollView(
            headerSliverBuilder: (
              BuildContext context,
              bool innerBoxIsScrolled,
            ) {
              return <Widget>[
                SliverAppBar(
                  leading: IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Theme.of(context).primaryColorDark,
                      size: 18,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  title: Center(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        right: 55,
                      ),
                      child: Text(
                        "CUSTOMIZE",
                        style: TextStyle(
                          color: Theme.of(context).primaryColorDark,
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                          fontFamily: 'Poppinss',
                          letterSpacing: 0,
                          height: 1,
                        ),
                      ),
                    ),
                  ),
                  actions: [],
                  expandedHeight: 260,
                  floating: false,
                  pinned: true,
                  backgroundColor: Theme.of(context).cardColor,
                  flexibleSpace: FlexibleSpaceBar(
                    background: Container(
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          colorFilter: new ColorFilter.mode(
                            Colors.black12.withOpacity(0.3),
                            BlendMode.dstATop,
                          ),
                          image: AssetImage(
                            'assets/images/download.jpg',
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ];
            },
            body: Container(
              color: Theme.of(context).primaryColor,
              width: double.infinity,
              height: double.infinity,
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.only(
                            left: 0,
                            right: 0,
                            bottom: 0,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 20,
                                  left: 20,
                                  right: 20,
                                  bottom: 10,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Food Fest Deal 2",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16,
                                        fontFamily: 'Poppins',
                                        letterSpacing: 0,
                                        height: 1,
                                      ),
                                    ),
                                    Text(
                                      "Rs. 279.00",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16,
                                        fontFamily: 'Poppins',
                                        letterSpacing: 0,
                                        height: 1,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 0,
                                  left: 20,
                                  right: 20,
                                  bottom: 10,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "6 Inch Chicken Sub Fajita And Drinks",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        fontFamily: 'Poppins',
                                        letterSpacing: 0,
                                        height: 1,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                child: Padding(
                                  padding: EdgeInsets.only(
                                    top: 5,
                                    bottom: 5,
                                    left: 20,
                                    right: 20,
                                  ),
                                  child: Divider(
                                    height: 1,
                                    thickness: 1,
                                    color: Theme.of(context).primaryColorDark,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 10,
                                  left: 20,
                                  right: 20,
                                  bottom: 10,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Choose Your Bread",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16,
                                        fontFamily: 'Poppins',
                                        letterSpacing: 0,
                                        height: 1,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(
                                        top: 5,
                                        bottom: 5,
                                        left: 8,
                                        right: 8,
                                      ),
                                      decoration: BoxDecoration(
                                          color: Theme.of(context).accentColor,
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(50),
                                              topLeft: Radius.circular(50),
                                              bottomRight: Radius.circular(50),
                                              bottomLeft: Radius.circular(50))),
                                      child: Text(
                                        "1 Requried",
                                        style: TextStyle(
                                            height: 1,
                                            fontSize: 12,
                                            fontFamily: 'Poppinss',
                                            letterSpacing: 0.3,
                                            fontWeight: FontWeight.w500,
                                            color: Theme.of(context)
                                                .primaryColorDark),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 0,
                                  left: 20,
                                  right: 20,
                                  bottom: 10,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Select 1",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        fontFamily: 'Poppins',
                                        letterSpacing: 0,
                                        height: 1,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      left: 20,
                                      right: 20,
                                      bottom: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Center(
                                                child: InkWell(
                                                  onTap: () {
                                                    setState(
                                                      () {
                                                        _value2 = !_value2;
                                                      },
                                                    );
                                                  },
                                                  child: Container(
                                                    width: 25,
                                                    height: 25,
                                                    padding:
                                                        const EdgeInsets.only(
                                                      left: 0,
                                                      top: 0,
                                                      right: 0,
                                                      bottom: 0,
                                                    ),
                                                    decoration: BoxDecoration(
                                                        shape:
                                                            BoxShape.rectangle,
                                                        color: Theme.of(context)
                                                            .hintColor,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(50),
                                                        border: Border.all(
                                                            color: Theme.of(
                                                                    context)
                                                                .buttonColor)),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        left: 0,
                                                        top: 0,
                                                        right: 0,
                                                        bottom: 0,
                                                      ),
                                                      child: _value2
                                                          ? Icon(
                                                              Icons.check,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColorDark,
                                                            )
                                                          : Icon(
                                                              Icons
                                                                  .check_box_outline_blank,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .cardColor,
                                                            ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                  left: 15,
                                                ),
                                                child: Text(
                                                  "Pepsi Small",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 16,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0,
                                                    height: 1,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Text(
                                          "+Rs. 0.00",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      left: 20,
                                      right: 20,
                                      bottom: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Center(
                                                child: InkWell(
                                                  onTap: () {
                                                    setState(
                                                      () {
                                                        _value2 = !_value2;
                                                      },
                                                    );
                                                  },
                                                  child: Container(
                                                    width: 25,
                                                    height: 25,
                                                    padding:
                                                        const EdgeInsets.only(
                                                      left: 0,
                                                      top: 0,
                                                      right: 0,
                                                      bottom: 0,
                                                    ),
                                                    decoration: BoxDecoration(
                                                        shape:
                                                            BoxShape.rectangle,
                                                        color: Theme.of(context)
                                                            .hintColor,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(50),
                                                        border: Border.all(
                                                            color: Theme.of(
                                                                    context)
                                                                .buttonColor)),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        left: 0,
                                                        top: 0,
                                                        right: 0,
                                                        bottom: 0,
                                                      ),
                                                      child: _value2
                                                          ? Icon(
                                                              Icons.check,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColorDark,
                                                            )
                                                          : Icon(
                                                              Icons
                                                                  .check_box_outline_blank,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .cardColor,
                                                            ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                  left: 15,
                                                ),
                                                child: Text(
                                                  "Pepsi Small",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 16,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0,
                                                    height: 1,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Text(
                                          "+Rs. 0.00",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 30,
                                  left: 20,
                                  right: 20,
                                  bottom: 10,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Choose Your Bread",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16,
                                        fontFamily: 'Poppins',
                                        letterSpacing: 0,
                                        height: 1,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(
                                        top: 5,
                                        bottom: 5,
                                        left: 8,
                                        right: 8,
                                      ),
                                      decoration: BoxDecoration(
                                          color: Theme.of(context).accentColor,
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(50),
                                              topLeft: Radius.circular(50),
                                              bottomRight: Radius.circular(50),
                                              bottomLeft: Radius.circular(50))),
                                      child: Text(
                                        "1 Requried",
                                        style: TextStyle(
                                            height: 1,
                                            fontSize: 12,
                                            fontFamily: 'Poppinss',
                                            letterSpacing: 0.3,
                                            fontWeight: FontWeight.w500,
                                            color: Theme.of(context)
                                                .primaryColorDark),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 0,
                                  left: 20,
                                  right: 20,
                                  bottom: 10,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Select 1",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        fontFamily: 'Poppins',
                                        letterSpacing: 0,
                                        height: 1,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      left: 20,
                                      right: 20,
                                      bottom: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Center(
                                                child: InkWell(
                                                  onTap: () {
                                                    setState(
                                                      () {
                                                        _value2 = !_value2;
                                                      },
                                                    );
                                                  },
                                                  child: Container(
                                                    width: 25,
                                                    height: 25,
                                                    padding:
                                                        const EdgeInsets.only(
                                                      left: 0,
                                                      top: 0,
                                                      right: 0,
                                                      bottom: 0,
                                                    ),
                                                    decoration: BoxDecoration(
                                                        shape:
                                                            BoxShape.rectangle,
                                                        color: Theme.of(context)
                                                            .hintColor,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(50),
                                                        border: Border.all(
                                                            color: Theme.of(
                                                                    context)
                                                                .buttonColor)),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        left: 0,
                                                        top: 0,
                                                        right: 0,
                                                        bottom: 0,
                                                      ),
                                                      child: _value2
                                                          ? Icon(
                                                              Icons.check,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColorDark,
                                                            )
                                                          : Icon(
                                                              Icons
                                                                  .check_box_outline_blank,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .cardColor,
                                                            ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                  left: 15,
                                                ),
                                                child: Text(
                                                  "Pepsi Small",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 16,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0,
                                                    height: 1,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Text(
                                          "+Rs. 0.00",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      left: 20,
                                      right: 20,
                                      bottom: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Center(
                                                child: InkWell(
                                                  onTap: () {
                                                    setState(
                                                      () {
                                                        _value2 = !_value2;
                                                      },
                                                    );
                                                  },
                                                  child: Container(
                                                    width: 25,
                                                    height: 25,
                                                    padding:
                                                        const EdgeInsets.only(
                                                      left: 0,
                                                      top: 0,
                                                      right: 0,
                                                      bottom: 0,
                                                    ),
                                                    decoration: BoxDecoration(
                                                        shape:
                                                            BoxShape.rectangle,
                                                        color: Theme.of(context)
                                                            .hintColor,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(50),
                                                        border: Border.all(
                                                            color: Theme.of(
                                                                    context)
                                                                .buttonColor)),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        left: 0,
                                                        top: 0,
                                                        right: 0,
                                                        bottom: 0,
                                                      ),
                                                      child: _value2
                                                          ? Icon(
                                                              Icons.check,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColorDark,
                                                            )
                                                          : Icon(
                                                              Icons
                                                                  .check_box_outline_blank,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .cardColor,
                                                            ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                  left: 15,
                                                ),
                                                child: Text(
                                                  "Pepsi Small",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 16,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0,
                                                    height: 1,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Text(
                                          "+Rs. 0.00",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 30,
                                  left: 20,
                                  right: 20,
                                  bottom: 10,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Choose Your Bread",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16,
                                        fontFamily: 'Poppins',
                                        letterSpacing: 0,
                                        height: 1,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(
                                        top: 5,
                                        bottom: 5,
                                        left: 8,
                                        right: 8,
                                      ),
                                      decoration: BoxDecoration(
                                          color: Theme.of(context).accentColor,
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(50),
                                              topLeft: Radius.circular(50),
                                              bottomRight: Radius.circular(50),
                                              bottomLeft: Radius.circular(50))),
                                      child: Text(
                                        "1 Requried",
                                        style: TextStyle(
                                            height: 1,
                                            fontSize: 12,
                                            fontFamily: 'Poppinss',
                                            letterSpacing: 0.3,
                                            fontWeight: FontWeight.w500,
                                            color: Theme.of(context)
                                                .primaryColorDark),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 0,
                                  left: 20,
                                  right: 20,
                                  bottom: 10,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Select 1",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        fontFamily: 'Poppins',
                                        letterSpacing: 0,
                                        height: 1,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      left: 20,
                                      right: 20,
                                      bottom: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Center(
                                                child: InkWell(
                                                  onTap: () {
                                                    setState(
                                                      () {
                                                        _value2 = !_value2;
                                                      },
                                                    );
                                                  },
                                                  child: Container(
                                                    width: 25,
                                                    height: 25,
                                                    padding:
                                                        const EdgeInsets.only(
                                                      left: 0,
                                                      top: 0,
                                                      right: 0,
                                                      bottom: 0,
                                                    ),
                                                    decoration: BoxDecoration(
                                                        shape:
                                                            BoxShape.rectangle,
                                                        color: Theme.of(context)
                                                            .hintColor,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(50),
                                                        border: Border.all(
                                                            color: Theme.of(
                                                                    context)
                                                                .buttonColor)),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        left: 0,
                                                        top: 0,
                                                        right: 0,
                                                        bottom: 0,
                                                      ),
                                                      child: _value2
                                                          ? Icon(
                                                              Icons.check,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColorDark,
                                                            )
                                                          : Icon(
                                                              Icons
                                                                  .check_box_outline_blank,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .cardColor,
                                                            ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                  left: 15,
                                                ),
                                                child: Text(
                                                  "Pepsi Small",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 16,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0,
                                                    height: 1,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Text(
                                          "+Rs. 0.00",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      left: 20,
                                      right: 20,
                                      bottom: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Center(
                                                child: InkWell(
                                                  onTap: () {
                                                    setState(
                                                      () {
                                                        _value2 = !_value2;
                                                      },
                                                    );
                                                  },
                                                  child: Container(
                                                    width: 25,
                                                    height: 25,
                                                    padding:
                                                        const EdgeInsets.only(
                                                      left: 0,
                                                      top: 0,
                                                      right: 0,
                                                      bottom: 0,
                                                    ),
                                                    decoration: BoxDecoration(
                                                        shape:
                                                            BoxShape.rectangle,
                                                        color: Theme.of(context)
                                                            .hintColor,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(50),
                                                        border: Border.all(
                                                            color: Theme.of(
                                                                    context)
                                                                .buttonColor)),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        left: 0,
                                                        top: 0,
                                                        right: 0,
                                                        bottom: 0,
                                                      ),
                                                      child: _value2
                                                          ? Icon(
                                                              Icons.check,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColorDark,
                                                            )
                                                          : Icon(
                                                              Icons
                                                                  .check_box_outline_blank,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .cardColor,
                                                            ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                  left: 15,
                                                ),
                                                child: Text(
                                                  "Pepsi Small",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 16,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0,
                                                    height: 1,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Text(
                                          "+Rs. 0.00",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 30,
                                  left: 20,
                                  right: 20,
                                  bottom: 10,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Choose Your Bread",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16,
                                        fontFamily: 'Poppins',
                                        letterSpacing: 0,
                                        height: 1,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(
                                        top: 5,
                                        bottom: 5,
                                        left: 8,
                                        right: 8,
                                      ),
                                      decoration: BoxDecoration(
                                          color: Theme.of(context).accentColor,
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(50),
                                              topLeft: Radius.circular(50),
                                              bottomRight: Radius.circular(50),
                                              bottomLeft: Radius.circular(50))),
                                      child: Text(
                                        "1 Requried",
                                        style: TextStyle(
                                            height: 1,
                                            fontSize: 12,
                                            fontFamily: 'Poppinss',
                                            letterSpacing: 0.3,
                                            fontWeight: FontWeight.w500,
                                            color: Theme.of(context)
                                                .primaryColorDark),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 0,
                                  left: 20,
                                  right: 20,
                                  bottom: 10,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Select 1",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        fontFamily: 'Poppins',
                                        letterSpacing: 0,
                                        height: 1,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      left: 20,
                                      right: 20,
                                      bottom: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Center(
                                                child: InkWell(
                                                  onTap: () {
                                                    setState(
                                                      () {
                                                        _value2 = !_value2;
                                                      },
                                                    );
                                                  },
                                                  child: Container(
                                                    width: 25,
                                                    height: 25,
                                                    padding:
                                                        const EdgeInsets.only(
                                                      left: 0,
                                                      top: 0,
                                                      right: 0,
                                                      bottom: 0,
                                                    ),
                                                    decoration: BoxDecoration(
                                                        shape:
                                                            BoxShape.rectangle,
                                                        color: Theme.of(context)
                                                            .hintColor,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(50),
                                                        border: Border.all(
                                                            color: Theme.of(
                                                                    context)
                                                                .buttonColor)),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        left: 0,
                                                        top: 0,
                                                        right: 0,
                                                        bottom: 0,
                                                      ),
                                                      child: _value2
                                                          ? Icon(
                                                              Icons.check,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColorDark,
                                                            )
                                                          : Icon(
                                                              Icons
                                                                  .check_box_outline_blank,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .cardColor,
                                                            ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                  left: 15,
                                                ),
                                                child: Text(
                                                  "Pepsi Small",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 16,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0,
                                                    height: 1,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Text(
                                          "+Rs. 0.00",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      left: 20,
                                      right: 20,
                                      bottom: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Center(
                                                child: InkWell(
                                                  onTap: () {
                                                    setState(
                                                      () {
                                                        _value2 = !_value2;
                                                      },
                                                    );
                                                  },
                                                  child: Container(
                                                    width: 25,
                                                    height: 25,
                                                    padding:
                                                        const EdgeInsets.only(
                                                      left: 0,
                                                      top: 0,
                                                      right: 0,
                                                      bottom: 0,
                                                    ),
                                                    decoration: BoxDecoration(
                                                        shape:
                                                            BoxShape.rectangle,
                                                        color: Theme.of(context)
                                                            .hintColor,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(50),
                                                        border: Border.all(
                                                            color: Theme.of(
                                                                    context)
                                                                .buttonColor)),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        left: 0,
                                                        top: 0,
                                                        right: 0,
                                                        bottom: 0,
                                                      ),
                                                      child: _value2
                                                          ? Icon(
                                                              Icons.check,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColorDark,
                                                            )
                                                          : Icon(
                                                              Icons
                                                                  .check_box_outline_blank,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .cardColor,
                                                            ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                  left: 15,
                                                ),
                                                child: Text(
                                                  "Pepsi Small",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 16,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0,
                                                    height: 1,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Text(
                                          "+Rs. 0.00",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 30,
                                  left: 20,
                                  right: 20,
                                  bottom: 10,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Choose Your Bread",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16,
                                        fontFamily: 'Poppins',
                                        letterSpacing: 0,
                                        height: 1,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(
                                        top: 5,
                                        bottom: 5,
                                        left: 8,
                                        right: 8,
                                      ),
                                      decoration: BoxDecoration(
                                          color: Theme.of(context).accentColor,
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(50),
                                              topLeft: Radius.circular(50),
                                              bottomRight: Radius.circular(50),
                                              bottomLeft: Radius.circular(50))),
                                      child: Text(
                                        "1 Requried",
                                        style: TextStyle(
                                            height: 1,
                                            fontSize: 12,
                                            fontFamily: 'Poppinss',
                                            letterSpacing: 0.3,
                                            fontWeight: FontWeight.w500,
                                            color: Theme.of(context)
                                                .primaryColorDark),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 0,
                                  left: 20,
                                  right: 20,
                                  bottom: 10,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Select 1",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        fontFamily: 'Poppins',
                                        letterSpacing: 0,
                                        height: 1,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      left: 20,
                                      right: 20,
                                      bottom: 0,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Center(
                                                child: InkWell(
                                                  onTap: () {
                                                    setState(
                                                      () {
                                                        _value2 = !_value2;
                                                      },
                                                    );
                                                  },
                                                  child: Container(
                                                    width: 25,
                                                    height: 25,
                                                    padding:
                                                        const EdgeInsets.only(
                                                      left: 0,
                                                      top: 0,
                                                      right: 0,
                                                      bottom: 0,
                                                    ),
                                                    decoration: BoxDecoration(
                                                        shape:
                                                            BoxShape.rectangle,
                                                        color: Theme.of(context)
                                                            .hintColor,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(50),
                                                        border: Border.all(
                                                            color: Theme.of(
                                                                    context)
                                                                .buttonColor)),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        left: 0,
                                                        top: 0,
                                                        right: 0,
                                                        bottom: 0,
                                                      ),
                                                      child: _value2
                                                          ? Icon(
                                                              Icons.check,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColorDark,
                                                            )
                                                          : Icon(
                                                              Icons
                                                                  .check_box_outline_blank,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .cardColor,
                                                            ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                  left: 15,
                                                ),
                                                child: Text(
                                                  "Pepsi Small",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 16,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0,
                                                    height: 1,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Text(
                                          "+Rs. 0.00",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      left: 20,
                                      right: 20,
                                      bottom: 20,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Center(
                                                child: InkWell(
                                                  onTap: () {
                                                    setState(
                                                      () {
                                                        _value2 = !_value2;
                                                      },
                                                    );
                                                  },
                                                  child: Container(
                                                    width: 25,
                                                    height: 25,
                                                    padding:
                                                        const EdgeInsets.only(
                                                      left: 0,
                                                      top: 0,
                                                      right: 0,
                                                      bottom: 0,
                                                    ),
                                                    decoration: BoxDecoration(
                                                        shape:
                                                            BoxShape.rectangle,
                                                        color: Theme.of(context)
                                                            .hintColor,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(50),
                                                        border: Border.all(
                                                            color: Theme.of(
                                                                    context)
                                                                .buttonColor)),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        left: 0,
                                                        top: 0,
                                                        right: 0,
                                                        bottom: 0,
                                                      ),
                                                      child: _value2
                                                          ? Icon(
                                                              Icons.check,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColorDark,
                                                            )
                                                          : Icon(
                                                              Icons
                                                                  .check_box_outline_blank,
                                                              size: 18,
                                                              color: Theme.of(
                                                                      context)
                                                                  .cardColor,
                                                            ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                  left: 15,
                                                ),
                                                child: Text(
                                                  "Pepsi Small",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 16,
                                                    fontFamily: 'Poppins',
                                                    letterSpacing: 0,
                                                    height: 1,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Text(
                                          "+Rs. 0.00",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            letterSpacing: 0,
                                            height: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        bottomNavigationBar: Container(
          color: Theme.of(context).cardColor,
          padding: EdgeInsets.only(
            top: 15,
            left: 20,
            right: 20,
            bottom: 15,
          ),
          child: Row(
            children: [
              Expanded(
                child: Row(
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Image.asset(
                        "assets/images/minus-sign.png",
                        width: 15,
                        height: 15,
                        color: Theme.of(context).buttonColor,
                      ),
                    ),
                    Text(
                      "1",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Theme.of(context).primaryColorDark,
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        fontFamily: 'Poppins',
                        letterSpacing: 0,
                        height: 1,
                      ),
                    ),
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Image.asset(
                        "assets/images/add.png",
                        width: 15,
                        height: 15,
                        color: Theme.of(context).buttonColor,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: SizedBox(
                  width: double.infinity,
                  height: 45,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.resolveWith<Color>(
                        (Set<MaterialState> states) {
                          return states.contains(MaterialState.pressed)
                              ? Theme.of(context).accentColor
                              : Theme.of(context).accentColor;
                        },
                      ),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                          side: BorderSide(
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => FoodDetailScreen(),
                        ),
                      );
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Add To Cart",
                          style: TextStyle(
                            color: Theme.of(context).primaryColorDark,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Poppins',
                            letterSpacing: 0.5,
                            fontSize: 13,
                            height: 1,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
