import 'package:flutter/material.dart';
import 'package:foodpanda/screens/common_screens/welcome_screen.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen();

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    var future = new Future.delayed(
      const Duration(milliseconds: 2500),
      () => {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => WelcomeScreen(),
          ),
        )
      },
    );

    return Scaffold(
      body: GestureDetector(
        onTap: () {},
        child: Image.asset(
          'assets/images/splash.png',
          height: double.infinity,
          width: double.infinity,
          alignment: Alignment.bottomCenter,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
