import 'package:flutter/material.dart';
import 'package:foodpanda/screens/common_screens/splash_screen.dart';
import 'package:hexcolor/hexcolor.dart';

void main() {
  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'foodpanda',
      theme: ThemeData(
        fontFamily: 'Poppins',
        primaryColor: Colors.black,
        primaryColorDark: Colors.white,
        buttonColor: Colors.grey[400],
        cardColor: HexColor("#121318"),
        hintColor: Colors.transparent,
        primaryColorLight: HexColor("#1c9ee6"),
        accentColor: HexColor("#f8a30e"),
      ),
      home: SplashScreen(),
    );
  }
}
